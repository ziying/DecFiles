# EventType: 12893300
#
# Descriptor: [ [B+]cc --> ^(D_s- => (K_S0 => pi+ pi-) K-) ... ]CC
# NickName: Bu_DsX,KSK=cocktail,TightCut,ACPKKCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Inclusive D_s => (K_S0 => pi pi) K produced in B+ decays, with cuts optimized for ACPKK
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# signal     = Generation().SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[ [B+]cc --> ^(D_s- => ^(KS0 => pi+ pi-) ^K-) ... ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV, GeV, micrometer ',
#     'import math',
#     'LL_KS0      = ( GNINTREE( ("pi+" == GABSID ) & in_range( 2 , GETA , 4.5 ) ) > 1.5 ) & ( GFAEVX(GVZ,0) > -100.0 * mm ) & ( GFAEVX(GVZ,0) < 500.0 * mm ) & ( GFAEVX(abs(GVX),0) < 40.0 * mm ) & ( GFAEVX(abs(GVY),0) < 40.0 * mm ) & GVEV',
#     'HarmCuts_Kp   = in_range (2 , GETA ,  4.5) & (GPT > 1.4 *GeV) & (GP > 4.9 *GeV)',
#     'HarmCuts_KS0  = in_range (2 , GETA ,  4.5)',
#     'HarmCuts_Dp  =  in_range (2 , GETA ,  4.5) & ( GPT > 2.9 *GeV )',
# ]		   
# tightCut.Cuts      =    {
#     '[K-]cc'         : 'HarmCuts_Kp',
#     'KS0'             : 'LL_KS0 & HarmCuts_KS0',
#     '[D_s-]cc'          : 'HarmCuts_Dp',
# }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Federico Betti
# Email: federico.betti@cern.ch
# Date: 20200910
# CPUTime: 2.5 min
#
#
Alias        MyK0s   K_S0
ChargeConj   MyK0s   MyK0s
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      MyD_s*+    D_s*+
Alias      MyD_s*-    D_s*-
ChargeConj MyD_s*+    MyD_s*-
#
Alias      MyD_s1+    D_s1+
Alias      MyD_s1-    D_s1-
ChargeConj MyD_s1-    MyD_s1+
#
Alias      MyD_s0*+   D_s0*+
Alias      MyD_s0*-   D_s0*-
ChargeConj MyD_s0*+   MyD_s0*-
#
Decay B+sig
 0.0003		MyD_s-		K+ mu+ nu_mu		PHSP;
 0.00029	MyD_s*-		K+ mu+ nu_mu		PHSP;
 0.000016	MyD_s+		pi0			PHSP;
 0.009		anti-D0		MyD_s+                  PHSP;
 0.0082		anti-D*0	MyD_s+                  SVS;
 0.0076		MyD_s*+		anti-D0                 SVS;
 0.0171		MyD_s*+   	anti-D*0                SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.0008 	anti-D0		MyD_s0*+		PHSP;
 0.0009 	anti-D*0	MyD_s0*+		SVS;
 0.0031		MyD_s1+		anti-D0                 SVS;
 0.012		anti-D*0 	MyD_s1+                 SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0;
 0.00018	MyD_s-    	pi+     K+              PHSP;
 0.000145	MyD_s*-   	pi+     K+              PHSP;
# BELOW 0.027 D_s(*) anti-D**0 -> assume D_s:D_s* = 1:2 and D**0:D1(2420)=D**0:D2(2460)=2:1
 0.0045		D_10		MyD_s+			SVS;
 0.0045		D_2*0		MyD_s+			STS;
 0.009		D_10		MyD_s*+			SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.009		MyD_s*+		D_2*0			PHSP;
Enddecay
CDecay B-sig
#
Decay MyD_s*+
  0.935   MyD_s+  gamma                VSP_PWAVE;
  0.058   MyD_s+  pi0                  VSS;
  0.0067  MyD_s+  e+ e-		       PHSP;
Enddecay
CDecay MyD_s*-
#
Decay MyD_s1+ # from PDG 2019
  0.48	MyD_s*+	pi0	 PARTWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.18	MyD_s+ gamma	 VSP_PWAVE;
  0.043	MyD_s+ pi+ pi-	 PHSP;
  0.08	MyD_s*+ gamma	 PHSP;
  0.037	MyD_s0*+ gamma	 PHSP;
Enddecay
CDecay MyD_s1-
#
Decay MyD_s0*+
  1.000   MyD_s+   pi0                  PHSP;
Enddecay
CDecay MyD_s0*-
#
Decay MyD_s+
  1.000        MyK0s         K+               PHSP;
Enddecay
CDecay MyD_s-
#
Decay MyK0s
  1.000        pi+           pi-              PHSP;
Enddecay
#
End
