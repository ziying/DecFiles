DecFiles v30r81 2022-12-13 
==========================  
 
! 2022-12-13 - Dmitrii Pereima (MR !1239)  
   Add new decay file  
   + 14143422 : Bc_jpsirho,mm=BcVegPy,TightCuts,BCVHAD2  
  
! 2022-12-13 - Haoqiang Zhao (MR !1237)  
   Add 2 new decay files  
   + 11196092 : Bd_LcXic,pKpi,pKpi=DecProdCut_pCut1600MeV  
   + 13196092 : Bs_LcXic,pKpi,pKpi=DecProdCut_pCut1600MeV  
  
! 2022-12-09 - Subrahmanya Pemmaraju (MR !1236)  
   Add 3 new decay files  
   + 11102264 : Bd_Ksta0,gg=DecProdCut,m=1000MeV  
   + 11102263 : Bd_Ksta0,gg=DecProdCut,m=180MeV  
   + 11102265 : Bd_Ksta0,gg=DecProdCut,m=3000MeV  
  
! 2022-12-07 - Matthew David Monk (MR !1234)  
   Modify decay file  
   + 11104043 : Bd_Kst0rho0,K+pi-pi+pi-=DecProdCut,AmpsFromRun1  
  
! 2022-12-06 - Vanya Belyaev (MR !1233)  
   Add new decay file  
   + 12307000 : Bu_D+D-KX=TightCuts,mD+D-  
  
! 2022-12-03 - Ibrahim Chahrour (MR !1232)  
   Add new decay file  
   + 22162010 : D0_Kpi=DecProdCut,NoPileUp,pthatmin15  
  
! 2022-12-01 - Vitalii Lisovskyi (MR !1230)  
   Add 3 new decay files  
   + 24114004 : incl_Jpsi,4m=DecProdCut,resonance30  
   + 24114003 : incl_Jpsi,4m=DecProdCut,resonance50  
   + 24114005 : incl_Jpsi,4m=DecProdCut,resonance70  
  
! 2022-12-01 - Mariusz Witek (MR !1229)  
   Add new decay file  
   + 25113200 : Lc_etap,mumugamma=TightCut  
  
! 2022-11-29 - Anfeng Li (MR !1228)  
   Add new decay file  
   + 11204000 : Bd_pi+pi-pi+pi-=DecProdCut,PHSP,Charmless,Cocktail  
  
! 2022-11-29 - Aravindhan Venkateswaran (MR !1227)  
   Modify decay file  
   + 12101010 : Bu_Ktautau,3pi3pi=DecProdCut,tauolacleo  
  
! 2022-11-29 - Mark Peter Whitehead (MR !1226)  
   Add 4 new decay files  
   + 11164024 : Bd_D0f2_1270,KK=SSD_CP,DecProdCut  
   + 11164034 : Bd_D0f2_1270,pipi=SSD_CP,DecProdCut  
   + 11164023 : Bd_D0rho0,KK=SSD_CP,DecProdCut  
   + 11164033 : Bd_D0rho0,pipi=SSD_CP,DecProdCut  
  
  
