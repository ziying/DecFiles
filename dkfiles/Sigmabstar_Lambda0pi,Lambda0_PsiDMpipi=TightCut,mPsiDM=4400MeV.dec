# EventType: 16503007
#
# Descriptor: [Sigma_b*- -> (Lambda_b0 -> pi+   pi-  H_30 ) pi-]cc
#
# NickName: Sigmabstar_Lambda0pi,Lambda0_PsiDMpipi=TightCut,mPsiDM=4400MeV
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#    Decay a L0 to pi pi and a redefined H_30 for our need, acting the latter as stable Dark Matter candidate. The L0 comes from a Sigma_b- redefined as a Sigma_b*-.
# EndDocumentation
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: 2 min
# Responsible: Saul Lopez
# Email: saul.lopez.solino@cern.ch
# Date: 20211122
#
#
# InsertPythonCode:
# from Configurables import LHCb__ParticlePropertySvc, LoKi__GenCutTool
# LHCb__ParticlePropertySvc().Particles = [
# "H_30       89       36      0.0     4.400000        1.000000e+16    A0                36    0.00",
# "Sigma_b-   114    5112     -1.0     5.83474000      8.776160e-23   Sigma_b-         5112    0.00",
# "Sigma_b~+  115   -5112      1.0     5.83474000      8.776160e-23   anti-Sigma_b+   -5112    0.00"
# ]
# ## Generator level cuts:
# from Configurables import LoKi__GenCutTool
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool, "TightCut" )
#
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^(Sigma_b- => (Lambda_b0 => pi+   pi-  H_30 ) pi-)]CC'
# ### - HepMC::IteratorRange::descendants   4
# tightCut.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodPi    = ( ( GPT > 0.40*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodLb    = ( ( 'Lambda_b0' == GABSID ) & ( GNINTREE( isGoodPi, 1 ) > 1 ) )"
#                          , "isGoodPiSig = ( ( GPT > 0.10*GeV ) & inAcc & ( 'pi+' ==GABSID) )"
#                          , "isGoodSigma = ( ( 'Sigma_b-' == GABSID ) & ( GNINTREE( isGoodLb, 1 ) > 0 ) & ( GNINTREE( isGoodPiSig, 1 ) > 0 ) )"]
# tightCut.Cuts = {
# "[Sigma_b-]cc" : "isGoodSigma"
# }
# EndInsertPythonCode
#
Alias  MyH_30     A0
Alias  Myanti-H_30    A0
ChargeConj MyH_30   Myanti-H_30
#
Alias        MyLambda_b0       Lambda_b0
Alias        Myanti-Lambda_b0  anti-Lambda_b0
ChargeConj   MyLambda_b0       Myanti-Lambda_b0
#
Decay Sigma_b-sig
    1.000  MyLambda_b0    pi-   PHSP;
Enddecay
CDecay anti-Sigma_b+sig
#
Decay MyLambda_b0
    1.000    pi-     pi+   MyH_30    PHSP;
Enddecay
CDecay Myanti-Lambda_b0
#
End
