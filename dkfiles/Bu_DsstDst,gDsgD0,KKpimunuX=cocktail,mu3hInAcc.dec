# EventType: 12875603
#
# Descriptor: [B+ ==> (D_s+ => K+ K- pi+) (anti-D0 => mu- anti-nu_mu ...) ...]cc
#
# NickName: Bu_DsstDst,gDsgD0,KKpimunuX=cocktail,mu3hInAcc
#
# Cuts: BeautyTomuCharmTo3h
#
# Documentation:
# Force B+ to combination of DsD0, Ds*D0, DsD*0, and Ds*D*0.
# In each case, force Ds to decay KKPi,
# then force D0 to decay semileptonically.
# For background study of semileptonic Bs->(Ds->KKpi)MuNu decays.
# EndDocumentation
#
# CPUTime: < 1 min
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Michael Wilkinson
# Email: miwilkin@syr.edu
# Date: 20201207
#
Alias      MyD_s+ D_s+
Alias      MyD_s- D_s-
ChargeConj MyD_s+ MyD_s-

Alias      MyD_s*+ D_s*+
Alias      MyD_s*- D_s*-
ChargeConj MyD_s*+ MyD_s*-

Alias      MyD0      D0
Alias      Myanti-D0 anti-D0
ChargeConj MyD0      Myanti-D0

Alias      MyD*0      D*0
Alias      Myanti-D*0 anti-D*0
ChargeConj MyD*0      Myanti-D*0

Alias      MyD_s1+    D_s1+
Alias      MyD_s1-    D_s1-
ChargeConj MyD_s1-    MyD_s1+

Alias      MyD'_s1+   D'_s1+
Alias      MyD'_s1-   D'_s1-
ChargeConj MyD'_s1-   MyD'_s1+

Alias      MyD_s0*+   D_s0*+
Alias      MyD_s0*-   D_s0*-
ChargeConj MyD_s0*+   MyD_s0*-
#
Decay B+sig
  0.0090 Myanti-D0  MyD_s+     PHSP;  # PDG2020
  0.0082 Myanti-D*0 MyD_s+     SVS;  # PDG2020
  0.0076 MyD_s*+    Myanti-D0  SVS;  # PDG2020
  0.0171 MyD_s*+    Myanti-D*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;  # PDG2020
  0.0010426 Myanti-D0  MyD_s0*+   PHSP;  # ratio to D_s*+ taken from 13874200
  0.0023458 Myanti-D*0 MyD_s0*+   SVS;  # ratio to D_s*+ taken from 13874200
  0.0006036 MyD_s1+    Myanti-D0  SVS;  # ratio to D_s*+ taken from 13874200
  0.0013581 MyD_s1+    Myanti-D*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;  # ratio to D_s*+ taken from 13874200, helamp taken from D_s*
  0.0006036 MyD'_s1+   Myanti-D0  SVS;  # ratio to D_s*+ taken from 13874200
  0.0013581 MyD'_s1+   Myanti-D*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;  # ratio to D_s*+ taken from 13874200, helamp taken from D_s*
Enddecay
CDecay B-sig
#
Decay MyD_s+
  0.0539 K+ K- pi+ PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
  0.935 MyD_s+ gamma VSP_PWAVE;
  0.058 MyD_s+ pi0   VSS;
Enddecay
CDecay MyD_s*-
#
Decay MyD0
  # the sum of the following is >> width of "mu + anything" in PDG2020
  0.018900000 K*-         mu+ nu_mu PHOTOS ISGW2; # PDG2020
  0.034100000 K-          mu+ nu_mu PHOTOS ISGW2; # PDG2020
  0.000760000 K_1-        mu+ nu_mu PHOTOS ISGW2; # PDG2020 for electron
  0.000802222 K_2*-       mu+ nu_mu PHOTOS ISGW2; # scaled to K_1- using ratio K_2*0 / K_10 above
  0.002670000 pi-         mu+ nu_mu PHOTOS ISGW2; # PDG2020
  0.001500000 rho-        mu+ nu_mu PHOTOS ISGW2; # PDG2020 for electron
  0.014400000 anti-K0 pi- mu+ nu_mu PHOTOS  PHSP; # PDG2020 for electron
  0.016000000 K-      pi0 mu+ nu_mu PHOTOS  PHSP; # PDG2020 for electron
  0.001450000 pi-     pi0 mu+ nu_mu PHOTOS  PHSP; # PDG2020 for electron
Enddecay
CDecay Myanti-D0
#
Decay MyD*0
  0.647 MyD0 pi0   VSS;
  0.353 MyD0 gamma VSP_PWAVE;
Enddecay
CDecay Myanti-D*0
#
Decay MyD_s1+
  0.480 MyD_s*+  pi0         PHOTOS PHSP;
  0.180 MyD_s+   gamma       PHOTOS PHSP;
  0.043 MyD_s+   pi+   pi-   PHOTOS PHSP;
  0.080 MyD_s*+  gamma       PHOTOS PHSP;
  0.037 MyD_s0*+ gamma       PHOTOS PHSP;
Enddecay
CDecay MyD_s1-
#
Decay MyD_s0*+
  1.0 MyD_s+ pi0 PHOTOS PHSP;
Enddecay
CDecay MyD_s0*-
#
Decay MyD'_s1+
  0.5 MyD_s*+ gamma     PHOTOS PHSP;
  0.5 MyD_s+  pi+   pi- PHOTOS PHSP;
Enddecay
CDecay MyD'_s1-
#
End
