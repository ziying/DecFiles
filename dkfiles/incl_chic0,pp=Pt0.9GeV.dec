# EventType: 28102033
#
# Descriptor: chi_c0(1P) -> anti-p- p+
#
# NickName: incl_chic0,pp=Pt0.9GeV
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: chi_c0 decay to p+ p- with phase space model, daughters in accpetance, and both protons PT>0.9 GeV
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool as GenCutTool
# #
# Generation().SignalPlain.addTool( GenCutTool , 'TightCut' )
# Generation().SignalPlain.TightCut.Decay = "chi_c0(1P) => ^p+ ^p~-"
# Generation().SignalPlain.TightCut.Cuts = {
#     'p+'  : ' ( GPT > 0.9 * GeV ) & inAcc ',
#     'p~-' : ' ( GPT > 0.9 * GeV ) & inAcc '
#     }
# Generation().SignalPlain.TightCut.Preambulo += [
#     'inAcc   = in_range ( 0.010 , GTHETA , 0.400 ) '
#     ]
#
# #
# # -- modify Pythia8 to only generate from Charmonium processes -- #
# from Configurables import Generation, MinimumBias, Pythia8Production, Inclusive, SignalPlain, SignalRepeatedHadronization, Special
#
# Pythia8TurnOffMinbias  = [ "SoftQCD:all     = off" ]
# Pythia8TurnOffMinbias += [ "Bottomonium:all = off" ]
# Pythia8TurnOffMinbias += [ "Charmonium:all  =  on" ]
#
# gen = Generation()
# gen.addTool( MinimumBias , name = "MinimumBias" )
# gen.MinimumBias.ProductionTool = "Pythia8Production"
# gen.MinimumBias.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.MinimumBias.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Inclusive , name = "Inclusive" )
# gen.Inclusive.ProductionTool = "Pythia8Production"
# gen.Inclusive.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Inclusive.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalPlain , name = "SignalPlain" )
# gen.SignalPlain.ProductionTool = "Pythia8Production"
# gen.SignalPlain.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalPlain.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalRepeatedHadronization , name = "SignalRepeatedHadronization" )
# gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
# gen.SignalRepeatedHadronization.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Special , name = "Special" )
# gen.Special.ProductionTool = "Pythia8Production"
# gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Special.Pythia8Production.Commands += Pythia8TurnOffMinbias
# # -- END  -- #
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Valeriia Zhovkovska
# Email: valeriia.zhovkovska@cern.ch
# Date: 20200422
# CPUTime: <1 min
#
Decay chi_c0sig
  1.000         p+        anti-p-        PHSP;
Enddecay
#
End
#
