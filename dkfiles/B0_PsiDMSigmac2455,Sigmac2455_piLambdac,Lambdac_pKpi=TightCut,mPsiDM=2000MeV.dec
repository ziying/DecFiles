# EventType: 11564002
#
# Descriptor: [B0 -> (Sigma_c0 -> (Lambda_c+ -> p+ K- pi+) pi-) H_30 ]cc
#
# NickName: B0_PsiDMSigmac2455,Sigmac2455_piLambdac,Lambdac_pKpi=TightCut,mPsiDM=2000MeV
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#   Decay a B0 to a Sigma_c(2455)0 -> pi Lambda_c+(-> pKpi) and a redefined H_30 for our need, acting the latter as stable Dark Matter candidate.
# EndDocumentation
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Saul Lopez
# Email: saul.lopez.solino@cern.ch
# Date: 20211111
#
#
# InsertPythonCode:
# from Configurables import LHCb__ParticlePropertySvc, LoKi__GenCutTool
# LHCb__ParticlePropertySvc().Particles = [
# "H_30     89       36      0.0     2.000000        1.000000e+16    A0      36      0.00"
# ]
# ## Generator level cuts:
# from Configurables import LoKi__GenCutTool
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool, "TightCut" )
#
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^(B0 => (Sigma_c0 => (Lambda_c+ => p+ K- pi+) pi-) H_30 )]CC'
# ### - HepMC::IteratorRange::descendants   4
# tightCut.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodKaon      = ( ( GPT > 0.20*GeV ) & inAcc & ( 'K+' == GABSID ) )"
#                          , "isGoodPiC       = ( ( GPT > 0.20*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodPi2455    = ( ( GPT > 0.20*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodP         = ( ( GPT > 0.20*GeV ) & inAcc & ( 'p+' == GABSID ) )"
#                          , "isGoodLc        = ( ( 'Lambda_c+' == GABSID ) & ( GNINTREE( isGoodKaon, 1 ) > 0 ) & ( GNINTREE( isGoodP, 1 ) > 0 ) & ( GNINTREE( isGoodPiC, 1 ) > 0 ) )"
#                          , "isGoodSc2455    = ( ( 'Sigma_c0' == GABSID ) & ( GNINTREE( isGoodLc, 1 ) > 0) & ( GNINTREE ( isGoodPi2455, 1 ) > 0 ) )"
#                          , "isGoodB         = ( ( 'B0' == GABSID ) & ( GNINTREE( isGoodSc2455, 1 ) > 0 ) )" ]
# tightCut.Cuts = {
# "[B0]cc" : "isGoodB"
# } 
# EndInsertPythonCode
#
Alias      MySigma_c0       Sigma_c0
Alias      Myanti-Sigma_c0       anti-Sigma_c0
ChargeConj MySigma_c0       Myanti-Sigma_c0
#
Alias       MyLambda_c+        Lambda_c+
Alias  Myanti-Lambda_c-        anti-Lambda_c-
ChargeConj      MyLambda_c+    Myanti-Lambda_c-
#
Alias  MyH_30     A0
ChargeConj MyH_30   MyH_30
#
Decay B0sig
    1.000   MySigma_c0   MyH_30    PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MySigma_c0
    1.000      MyLambda_c+ pi-             PHSP;
Enddecay
CDecay Myanti-Sigma_c0
#
Decay MyLambda_c+
    1.000       p+  K-  pi+      PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
End
