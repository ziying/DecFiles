# EventType: 11166081
#
# Descriptor: [B0 -> Myanti-Lambda_c- p+ pi- pi+]cc
#
# NickName: Bd_Lcpipip,TightCut
#
# Cuts: DaughtersInLHCb
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B0 ==> (Lambda_c~- ==> ^p~- ^K+ ^pi-) ^p+ ^pi- ^pi+]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 )",
# "goodKpi = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV) & InAcc",
# "goodp = ( GP > 5000 * MeV ) & ( GPT > 200 * MeV ) & InAcc"
#]
#tightCut.Cuts = {
#'[pi-]cc' : "goodKpi",
#'[K-]cc' : "goodKpi",
#'[p+]cc' : "goodp"
#}
#
# EndInsertPythonCode
#
# Documentation: This is the decay file for the decay B0 -> (anti-Lambda_c- -> p~- K+ pi-) p + pi- pi+.
# The B0 is forced to decay hadronically to Lambda_c- p+ pi- pi+.
# The Lambda_c is forced to the p+ K- pi+ final state.
# All charged final state tracks are required to be within the LHCb acceptance.
# EndDocumentation
#
# CPUTime: < 1 min
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: La Wang
# Email: wangla@hun.edu.cn
# Date: 20210306
#

# Define Lambda_c
Alias      MyLambda_c+      Lambda_c+
Alias      Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+      Myanti-Lambda_c-


# Define B0 decay
Decay B0sig
  1.000 Myanti-Lambda_c- p+  pi- pi+ PHSP;
Enddecay
CDecay anti-B0sig

# Define Lambda_c+ decay
Decay MyLambda_c+
  1.000 p+              K-         pi+ PHSP;
Enddecay
CDecay Myanti-Lambda_c-

End
