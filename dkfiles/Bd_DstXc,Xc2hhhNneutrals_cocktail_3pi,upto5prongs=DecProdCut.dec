# EventType: 11896623
#
# Descriptor: {[B0 -> (D*- -> pi- (anti-D0 -> K+ pi-)) D_s+ ... ]cc}
# NickName: Bd_DstXc,Xc2hhhNneutrals_cocktail_3pi,upto5prongs=DecProdCut
#
# Cuts: LoKi::GenCutTool/TightCut
# ParticleValue: "eta(1475) 826 100331 0.0 1.47500000 7.743673e-24 eta(1475) 100331 0.00000000"
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# tightCut = gen.TightCut
# tightCut.Decay = "[(Beauty & LongLived) --> ^(D*(2010)+ => ^(D0 => K- pi+) pi+) ...]CC"
# tightCut.Cuts = {
#     "(Beauty & LongLived)" : "good_B",
#     "[D*(2010)+]cc" : "good_Dstar",
#     "[D0]cc" : "good_D0",
# }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV, GeV",
#     "inAcc = ( GPZ > 0 ) & ( GPT > 40 * MeV ) & ( GP > 1.6 * GeV ) & in_range ( 1.8, GETA, 5.0 ) & in_range ( 0.005, GTHETA, 0.400 )",
#     "nPi = GCOUNT(('pi+' == GABSID) & inAcc, HepMC.descendants)",
#     "nK  = GCOUNT(('K-'  == GABSID) & inAcc, HepMC.descendants)",
#     "good_slow_pion = ('pi+' == GABSID) & (GPT >  40 * MeV) & inAcc",
#     "good_D0_pion   = ('pi+' == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0_kaon   = ('K+'  == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0        = ('D0'  == GABSID) & (GPT > 1.5 * GeV) & ( GP > 15. * GeV ) & GCHILDCUT(good_D0_kaon, 'Charm => ^(K+|K-) (pi+|pi-)') & GCHILDCUT(good_D0_pion, 'Charm => (K+|K-) ^(pi+|pi-)')",
#     "good_Dstar     = ('D*(2010)+' == GABSID) & GCHILDCUT(good_slow_pion, 'Charm => Charm ^(pi+|pi-)')",
#     "good_B         = (nK >= 1) & (nPi >= 3)",
# ]
# EndInsertPythonCode

# Documentation: B0 -> D* Ds X cocktail, where Ds decays to final states with at least 3 charged pions and may come from D_s*, D_s*(2317), D_s(2457) or D_s(2536) and D* may come from D_1, D_1', D_2*. For B0 decays to D**Ds(*) states, all BR is not well measured and the values are set according to the information from LHCb-ANA-2019-026 and a study of B0 -> D*Dspi: https://indico.cern.ch/event/737951/.  For Ds decays, some decay modes with an indeterminate BR in PDG are set a small BR for possible needs of template. These modes include 3pi(NR), 5pi(NR), f0pi, f2pi, f'_0pi, f0(1500)pi, a1etap, a1a0, a1f0, a03pi, etap3pi, 5pipi0(NR), 5pi2pi0(NR), 3pipi0(NR).
#
# Background for B2XTauNu analyses.
# Myphi = 0.1524*phi
# Myomega = 0.9073*omega
# Myeta = 0.2714*eta
# Myetap = 0.8037*eta'
# Mya1+ = 0.5*a_1+
# EndDocumentation
#
# CPUTime: <1 min 
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Bo Fang
# Email: bo.fang@cern.ch
# Date: 20220414
#
Alias           Myphi          phi
ChargeConj      Myphi          Myphi
#
Alias           Myomega        omega
ChargeConj      Myomega        Myomega
#
Alias           Myeta          eta
ChargeConj      Myeta          Myeta
#
Alias           Myetap         eta'
ChargeConj      Myetap         Myetap
#
Alias           Mysigma0       sigma_0
ChargeConj      Mysigma0       Mysigma0
#
Alias           Myf0           f_0
ChargeConj      Myf0           Myf0
#
Alias           Mya00          a_00
ChargeConj      Mya00          Mya00
#
Alias           Mya0+          a_0+
Alias           Mya0-          a_0-
ChargeConj      Mya0+          Mya0-
#
Alias           Myf2           f_2
ChargeConj      Myf2           Myf2
#
Alias           Myf'_0         f'_0
ChargeConj      Myf'_0         Myf'_0
#
Alias           Myf0(1500)     f_0(1500)
ChargeConj      Myf0(1500)     Myf0(1500)
#                                
Alias           Myf1(1420)0    f'_1
ChargeConj      Myf1(1420)0    Myf1(1420)0
#
Alias           Myeta(1475)0   eta(1475)
ChargeConj      Myeta(1475)0   Myeta(1475)0
#
Alias           Mya1+          a_1+
Alias           Mya1-          a_1-
ChargeConj      Mya1+          Mya1-
#
Alias           MyD_s+         D_s+
Alias           MyD_s-         D_s-
ChargeConj      MyD_s+         MyD_s-
#
Alias           MyD_s*+        D_s*+
Alias           MyD_s*-        D_s*-
ChargeConj      MyD_s*+        MyD_s*-
#
Alias           MyD_s*(2317)+  D_s0*+
Alias           MyD_s*(2317)-  D_s0*-
ChargeConj      MyD_s*(2317)+  MyD_s*(2317)-
#
#
Alias           MyD_s*(2457)+  D_s1+
Alias           MyD_s*(2457)-  D_s1-
ChargeConj      MyD_s*(2457)+  MyD_s*(2457)-
#
Alias           MyD_s*(2536)+  D'_s1+
Alias           MyD_s*(2536)-  D'_s1-
ChargeConj      MyD_s*(2536)+  MyD_s*(2536)-
#
Alias           MyMainD*+      D*+
Alias           MyMainD*-      D*-
ChargeConj      MyMainD*+      MyMainD*-
#
Alias           MyD0           D0
Alias           anti-MyD0      anti-D0
ChargeConj      MyD0           anti-MyD0
#
Alias           Mytau+         tau+
Alias           Mytau-         tau-
ChargeConj      Mytau+         Mytau-
#
Alias           MyD'_1-        D'_1-
Alias           MyD'_1+        D'_1+
ChargeConj      MyD'_1-        MyD'_1+
#
Alias           MyD_1-         D_1-
Alias           MyD_1+         D_1+
ChargeConj      MyD_1-         MyD_1+
#
Alias           MyD_2*-        D_2*-
Alias           MyD_2*+        D_2*+
ChargeConj      MyD_2*-        MyD_2*+
#
#
Decay B0sig
# arxiv 2105.02596 Bd->D*-D_s*+ H+ 0.620 phi+ -0.046 H0 0.760 H- 0.195 phi- 0.108  use the same helicity info for Ds*(2457)+ and Ds*(2536)+.
#0.0177           MyMainD*-       MyD_s*+         SVV_HELAMP 0.620 -0.046 0.760 0.0 0.195 0.108;                 
#0.0015           MyMainD*-       MyD_s*(2317)+   SVS;                                                           
#0.0093           MyMainD*-       0.765*D_s*(2457)+   SVV_HELAMP 0.620 -0.046 0.760 0.0 0.195 0.108;             
# No scale        MyMainD*-       D_s*(2536)+                                                                         
#0.0005           MyMainD*-       D_s*(2536)+     SVV_HELAMP 0.620 -0.046 0.760 0.0 0.195 0.108;                 
 0.0080           MyMainD*-       MyD_s+          SVS;                                                           
 0.0177           MyMainD*-       MyD_s*+         SVV_HELAMP 0.620 -0.046 0.760 0.0 0.195 0.108;                 
 0.0015           MyMainD*-       MyD_s*(2317)+   SVS;                                                           
 0.0071           MyMainD*-       MyD_s*(2457)+   SVV_HELAMP 0.620 -0.046 0.760 0.0 0.195 0.108;                 
 0.0005           MyMainD*-       MyD_s*(2536)+   SVV_HELAMP 0.620 -0.046 0.760 0.0 0.195 0.108;                 
                                                                                                                           
# Include charged D** states                                                                                        
#                                                                                                                       
#### B0 decays containing D* excited and Ds excited states                                                   
#### From LHCb-ANA-2019-026, one can get BRs of D**Ds decays; from the study of B0 -> D*Dspi(https://indico.cern.ch/event/737951/), one can get BRs of D**Ds* decays. From fig.36 in LHCb-ANA-2019-026, supposing (1)\sum{BR(D**Ds**)} = 0.2\sum{BR(D**Ds*)} (2)decays containing different D** state follow the ratio in B0->D**Ds decays and (3)decays containing different Ds** state follow the ratio in B0->D*Ds** decays, then derive all BRs.
 0.000316         MyD'_1-         MyD_s+          SVS;                                                        
 0.000484         MyD'_1-         MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;               
 0.000010         MyD'_1-         MyD_s*(2317)+   SVS;                                                        
 0.000047         MyD'_1-         MyD_s*(2457)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;                         
 0.000003         MyD'_1-         MyD_s*(2536)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;                         
 #                                                                                                            
 0.001            MyD_1-          MyD_s+          SVS;                                    
 0.000847         MyD_1-          MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;                
 0.00003          MyD_1-          MyD_s*(2317)+   SVS;                                                        
 0.000148         MyD_1-          MyD_s*(2457)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;                         
 0.000010         MyD_1-          MyD_s*(2536)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;                         
#### below devided by 3 compared to B+ -> D2*0 Ds X to account for ratio to D* pi                               
#0.000152         0.33*D_2*-         MyD_s+          STS;                                                     
#0.000248         0.33*D_2*-         MyD_s*+         PHSP;                                                    
 0.000051         MyD_2*-         MyD_s+          STS;                                                        
 0.000083         MyD_2*-         MyD_s*+         PHSP;                                                       
 0.000002         MyD_2*-         MyD_s*(2317)+   STS;                                                        
 0.000007         MyD_2*-         MyD_s*(2457)+   PHSP;                                                       
 0.000001         MyD_2*-         MyD_s*(2536)+   PHSP;                                                       
#### Add some NR D*pi contribution accounting for higher mass D** states and NR                              
 0.000126         MyMainD*-     pi0         MyD_s+           PHSP;                                            
 0.000094         MyMainD*-     pi0         MyD_s*+          PHSP;
 0.000004         MyMainD*-     pi0         MyD_s*(2317)+    PHSP;
 0.000019         MyMainD*-     pi0         MyD_s*(2457)+    PHSP;
 0.000001         MyMainD*-     pi0         MyD_s*(2536)+    PHSP;
Enddecay
CDecay anti-B0sig

Decay MyD_s+
 0.0108         pi+     pi-     pi+       D_DALITZ;
#0.0079         pi+    pi-    pi+    pi-    pi+
 0.00260        pi+  pi-  pi+  pi-  pi+   PHSP;
 0.00038        Myf'_0          pi+       PHSP;
 0.00302        Myf0(1500)      pi+       PHSP;

#0.045          0.1524*phi      pi+       SVS;
#0.0559         0.1524*phi      rho+      SVV;
 0.00686        Myphi           pi+       SVS;
 0.00852        Myphi           rho+      SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;

# arxiv 0906.2138 page7 caption of Fig3 0.52+/-0.30 rho+
#0.00192       0.9073*omega    pi+             SVS;
#0.028*0.52    0.9073*omega    rho+            SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
#0.028*0.48    0.9073*omega    pi+     pi0     PHSP;
 0.00174       Myomega      pi+           SVS;
 0.01321       Myomega      rho+          SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.01219       Myomega      pi+     pi0   PHSP;

#0.0168        0.2714*eta    pi+          PHSP;
#0.0744        rho+        0.2714*eta     SVS;
#0.0051        0.2714*eta    pi+     pi0  PHSP;
 0.00456       Myeta       pi+            PHSP;
 0.02019       rho+        Myeta          SVS;
 0.00138       Myeta       pi+     pi0    PHSP;

#0.0394        0.8037*etap     pi+        PHSP;
#0.058         rho+        0.8037*etap    SVS;
#0.002         0.8037*etap     pi+   pi0  PHSP;
 0.03167       Myetap      pi+            PHSP;
 0.04661       rho+        Myetap         SVS;
 0.00161       Myetap      pi+     pi0    PHSP;

#### a0 -> 0.2714*eta pi
 0.00299       Mya0+       pi0            PHSP;
 0.00299       Mya00       pi+            PHSP;
# arxiv 2106.13536 Supposing BR(Ds+ -> a_0^+ rho^0) = BR(Ds+ -> a_0^0 rho^+)
 0.00057       rho0        Mya0+          SVS;
 0.00057       rho+        Mya00          SVS;

#### a0 -> 0.2714*eta pi
 0.00043    Mya0-     pi+     pi+         PHSP;
 0.00029    Mya0+     pi-     pi+         PHSP;
 0.00193    Myf0      eta     pi+         PHSP;
 0.00396    Mysigma0  eta     pi+         PHSP;
 0.0085     omega     eta     pi+         PHSP;


#0.00057/(3.46/0.77)      Myf1(1420)0       pi+     SVS;
#0.00057/(3.46/1.37)      Myeta(1475)0      pi+     PHSP;
 0.00013      Myf1(1420)0       pi+       SVS;
 0.00023      Myeta(1475)0      pi+       PHSP;


#0.0312*(0.554+0.081)           Mya1+           eta         SVS;
#0.0074*2        Mya1+                 phi                  SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.0198          Mya1+                 eta                  SVS;
 0.004           Mya1+                 eta'                 SVS;
 0.008           Mya1+                 omega                SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.0148          Mya1+                 phi                  SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.003           Mya1+                 K0                   SVS;
 0.0015          Mya1+                 a_00                 SVS;
 0.0015          a_00        pi+     pi-     pi+            PHSP;
 0.003           Mya1+               Myf0                   SVS;
 0.002           Mya1+               Myf2                   PHSP;
 0.004           eta'        pi+     pi-     pi+            PHSP;
 0.008           omega       pi+     pi-     pi+            PHSP;
 0.0036          phi         pi+     pi-     pi+            PHSP;
 0.00084         K+    K-       pi+     pi-     pi+         PHSP;
 0.00336         K0    anti-K0    pi+     pi-     pi+       PHSP;
 0.003           K0          pi+     pi-     pi+            PHSP;


 0.0005         pi+  pi-  pi+  pi-  pi+  pi0          PHSP;
 0.0001         pi+  pi-  pi+  pi-  pi+  pi0  pi0     PHSP;
##from Alessandra in decfile BR 0.0109 for 3pi, 0.015 for 3pipi0, weight are 1.03 for 3pi and 0.56 for 3pipi0
 0.010          pi+     pi-     pi+     pi0           PHSP;

#0.0543         0.14*tau+       nu_tau          SLN;
 0.0076         Mytau+          nu_tau          SLN;

Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
 0.942          MyD_s+  gamma                   VSP_PWAVE;
 0.058          MyD_s+  pi0                     PHSP;
Enddecay
CDecay MyD_s*-
#
Decay MyD_s*(2317)+
 1.000          MyD_s+  pi0                     PHSP;
Enddecay
CDecay MyD_s*(2317)-
#
Decay MyD_s*(2457)+
 0.18           MyD_s+          gamma           VSP_PWAVE;
 0.48           MyD_s*+         pi0             PHSP;
 0.043          MyD_s+          pi+     pi-     PHSP;
 0.022          MyD_s+          pi0     pi0     PHSP;
 0.04           MyD_s*(2317)+   gamma           VSP_PWAVE;
Enddecay
CDecay MyD_s*(2457)-
#
Decay MyD_s*(2536)+
 0.25           MyD_s+          pi+     pi-     PHSP;
 0.125          MyD_s+          pi0     pi0     PHSP;
 0.1            MyD_s*+         gamma           PHSP;
Enddecay
CDecay MyD_s*(2536)-
#
Decay MyMainD*+
 1.000          MyD0    pi+                     VSS;
Enddecay
CDecay MyMainD*-
#
SetLineshapePW D_1+ D*+ pi0 2
SetLineshapePW D_1+ D*0 pi+ 2
SetLineshapePW D_1- D*- pi0 2
SetLineshapePW D_1- anti-D*0 pi- 2
#
SetLineshapePW D_2*+ D*+ pi0 2
SetLineshapePW D_2*+ D*0 pi+ 2
SetLineshapePW D_2*- D*- pi0 2
SetLineshapePW D_2*- anti-D*0 pi- 2
#
Decay MyD_1-
 1.000          MyMainD*-       pi0             VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyD_1+
#
Decay MyD'_1-
 1.000          MyMainD*-       pi0             VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1+
#
Decay MyD_2*-
 1.000          MyMainD*-       pi0             TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyD_2*+
#
Decay MyD0
 1.000          K-      pi+                     PHSP;
Enddecay
CDecay anti-MyD0
#
Decay Mytau+
 0.09                                           TAUOLA 5;
 0.05                                           TAUOLA 8;
Enddecay
CDecay Mytau-
#
Decay Mysigma0
 1.000          pi+     pi-                     PHSP;
Enddecay
#
Decay Myeta
 0.2292         pi-     pi+     pi0             ETA_DALITZ;
 0.0422         pi-     pi+     gamma           PHSP;
Enddecay
#
Decay Myomega
 0.892          pi-     pi+     pi0             OMEGA_DALITZ;
 0.0153         pi-     pi+                     PHSP;
Enddecay
#
Decay Myetap
 0.295          rho0            gamma           SVP_HELAMP 1.0 0.0 1.0 0.0;
 0.425          eta             pi+     pi-     PHSP;
#0.0252         0.9073*omega    gamma           PHSP;
#0.224          0.2714*eta      pi0     pi0     PHSP;
 0.0229         Myomega         gamma           PHSP;
 0.0608         Myeta           pi0     pi0     PHSP;
Enddecay
#
Decay Myphi
 0.1524         pi+     pi-     pi0             PHI_DALITZ;
Enddecay
#
Decay Mya00
#1.000          0.2714*eta     pi0              PHSP; 
 0.2714         Myeta          pi0              PHSP; 
Enddecay
#
Decay Mya0+
#1.000          0.2714*eta     pi+              PHSP; 
 0.2714         Myeta          pi+              PHSP; 
Enddecay
CDecay Mya0-
#
Decay Myf1(1420)0
 0.333          Mya00          pi0              PHSP;
 0.333          Mya0+          pi-              PHSP;
 0.333          Mya0-          pi+              PHSP;
Enddecay
#
Decay Myeta(1475)0
 0.333          Mya00          pi0              PHSP;
 0.333          Mya0+          pi-              PHSP;
 0.333          Mya0-          pi+              PHSP;
Enddecay
#
Decay Mya1+
#0.554/(0.554+0.081)           rho0             pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
#0.081/(0.554+0.081)           Mysigma0         pi+ VSS;
 0.8724         rho0           pi+              VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
 0.1276         Mysigma0       pi+              VSS;
Enddecay
CDecay Mya1-
#
Decay Myf0
 1.000          pi+     pi-                     PHSP;
Enddecay
#
Decay Myf2
 0.565          pi+     pi-                     PHSP;
 0.028          pi+     pi-   pi+    pi-        PHSP;
Enddecay
#
Decay Myf'_0
 0.075          pi+     pi-    pi+    pi-       PHSP;
Enddecay
#
Decay Myf0(1500)
 0.354          pi+     pi-    pi+    pi-       PHSP;
Enddecay
#
End
