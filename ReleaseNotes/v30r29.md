
!========================= DecFiles v30r29 2019-03-07 =======================

! 2019-02-20 - Andrii Usachov (MR !231)  
  new decfiles and changes in old ones for charmonium production  
  + 10132060 : incl_b=etac1S,ppbar,InAcc,PTCut.dec - fix evttype, add cuts  
  + 10132080 : incl_b=etac2S,ppbar,InAcc,PTCut.dec - new decfile  
  + 10134000 : incl_b=Jpsi,LstLst,pK,InAcc.dec - fix evttype, add cuts  
  + 10134001 : incl_b=psi2S,LstLst,pK,InAcc.dec - fix evttype, add cuts  
  + 10134060 : incl_b=etac1S,phiphi,KK,InAcc.dec - fix evttype, add cuts  
  + 10134080 : incl_b=etac2S,phiphi,KK,InAcc.dec - fix evttype, add cuts  
  + 10334100 : incl_b=Jpsi,LstLambda,pKppi,InAcc.dec - fix evttype, add cuts  
  + 10334101 : incl_b=psi2S,LstLambda,pKppi,InAcc.dec - fix evttype, add cuts  
  + 24102010 : incl_etac,pp=TightCut.dec - tighten cuts  
  + 24104010 : incl_etac,phiphi,KK=TightCut.dec - new decfile  
  + 28102001 : incl_etac2S,pp=TightCut.dec - new decfile  
  + 28104003 : incl_etac2S,phiphi,KK=TightCut.dec - new decfile  
  
! 2019-03-06 - Alison Tully (MR !245)  
   Add 4 new decfiles  
   + 14573042 : Bc_D0munu,Kmunu=BcVegPy,DecProdCut,ffEbert.dec  
   + 14573032 : Bc_D0munu,Kmunu=BcVegPy,DecProdCut,ffKiselev.dec  
   + 14773442 : Bc_Dst0munu,Kmunu=BcVegPy,DecProdCut,ffEbert.dec  
   + 14773432 : Bc_Dst0munu,Kmunu=BcVegPy,DecProdCut,ffKiselev.dec  
  
! 2019-03-01 - Svende Braun (MR !241)  
   Removed hats from decay descriptor and updated Branching fractions and  
   K* decays for Bs->Kmunu analysis: modified 2 previously unused dec files, 2 new dec files, declared old ones as obsolete  
   + 11443022 : Bd_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 12445022 : Bu_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 13444022 : Bs_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 15144062 : Lb_JpsiKp,mumu,PPTcuts=TightCut.dec  
  
! 2019-03-03 - Brij Kishor Jashal (MR !242)  
   Add new decay file  
   + 15102320 : Lb_gammaLambda=HighPtGamma.dec  
  
! 2019-02-28 - Luke Scantlebury-Smead (MR !239)  
   Added new decfiles for Bu -> Dstst0 Tau Nu study  
   + 12567001 : Bu_Dstst0taunu,tau3pi,D_10=Dst+pi-,Dst+=D0pi+,TightCut.dec  
   + 12567002 : Bu_Dstst0taunu,tau3pi,D_1H0=Dst+pi-,Dst+=D0pi+,TightCut.dec  
   + 12567003 : Bu_Dstst0taunu,tau3pi,D_20=Dst+pi-,Dst+=D0pi+,TightCut.dec  
  
! 2019-02-12 - Carla Marin (MR !222)
   Modify decfile 15102213 for proper usage of decay arrows,   
   update event type to 15102214 and add old one to table_obsolete  
   + 15102214 : Lb_gammapK=HighPtGamma,DecProdCut.dec  
  
! 2019-03-04 - Florian Reiss (MR !235)
   add 20 new decfiles for B0->D*+munu/B0->D*+enu relative branching  
   fraction measurement with tighter generator level cuts:  
   + 11574091 : Bd_Dst+munu=PHSP,TightCut,tighter.dec  
   + 12875060 : Bu_DststXmunu,Dst+=cocktail,TightCut,tighter.dec  
   + 11874061 : Bd_DststXmunu,Dst+=cocktail,TightCut,tighter.dec  
   + 11874271 : Bd_Dst+DsX,taunu=cocktail,TightCut,tighter.dec  
   + 12675440 : Bu_Dst+DX,muX=cocktail,TightCut,tighter.dec  
   + 12875050 : Bu_Dst+DsX,taunu=cocktail,TightCut,tighter.dec  
   + 11874650 : Bd_Dst+DX,muX=cocktail,TightCut,tighter.dec  
   + 11676011 : Bd_Dststmunu,Dst+=CocktailHigher,TightCut,tighter.dec  
   + 12675401 : Bu_Dststmunu,Dst+=CocktailHigher,TightCut,tighter.dec  
   + 13874010 : Bs_Dsststmunu,Dst+=cocktail,TightCut,tighter.dec  
   + 11584031 : Bd_Dst+enu=PHSP,TightCut,tighter.dec  
   + 12885000 : Bu_DststXenu,Dst+=cocktail,TightCut,tighter.dec  
   + 11686000 : Bd_Dststenu,Dst+=CocktailHigher,TightCut,tighter.dec  
   + 12685400 : Bu_Dststenu,Dst+=CocktailHigher,TightCut,tighter.dec  
   + 11884650 : Bd_Dst+DX,eX=cocktail,TightCut,tighter.dec  
   + 12685410 : Bu_Dst+DX,eX=cocktail,TightCut,tighter.dec  
   + 11884271 : Bd_Dst+DsX,taunuenu=cocktail,TightCut,tighter.dec  
   + 12885031 : Bu_Dst+DsX,taunuenu=cocktail,TightCut,tighter.dec  
   + 13884000 : Bs_Dsststenu,Dst+=cocktail,TightCut,tighter.dec  
   + 11584050 : Bd_DststXenu,Dst+=cocktail,TightCut,tighter.dec  
  
! 2019-02-19 - Dima Popov (MR !230)  
   Update decay files to follow changes in Gauss  
   + 59984000 : ContinuumDD,KPi,KPi=NoCut  
   + 59990001 : continuum_pi+pi-  
   + 59990002 : continuum_mu+mu-  
  
! 2019-02-26 - David Gerick (MR !234)  
   Modified 2 new/unused decay files:  
   + 12115179 : Bu_Kstmumu,KSpi=PHSP,flatq2,DecProdCut,TightCut.dec  
   + 12113446 : Bu_Kstmumu,Kpi0=PHSP,flatq2,DecProdCut,TightCut.dec  
  
! 2019-03-01 - Svende Braun (MR !241)  
   Removed hats from decay descriptor and updated Branching fractions and K* decays for Bs->Kmunu analysis: modified 2 previously unused dec files, 2 new dec files, declared old ones as obsolete  
   + 11443022 : Bd_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 12445022 : Bu_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 13444022 : Bs_CharmoniumKX,mumu,PPTcuts=TightCut.dec  
   + 15144062 : Lb_JpsiKp,mumu,PPTcuts=TightCut.dec  
  
! 2019-02-28 - Xixin Liang (MR !240)  
   Added 1 new decay file  
   + 15874101 : Lb_Lcmunu,LambdaPi=cocktail,Lb2Baryonlnu.dec  
  
! 2019-02-28 Adam Morris (MR !237)  
   Modify 4 decay files:  
   + 11996412 : Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut.dec  
   + 11996413 : Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut,TightCut.dec  
   + 12997612 : B+_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut.dec  
   + 12997613 : B+_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut,TightCut.dec  
   Add missing CDecay statements to D2* D1 and D1' states  
   Change name B+_DstXc -> B+_excitedDstXc to be more accurate  
   Loosen slow pion pT cut to 40 MeV in TightCut files (reflects change in stripping)  
   Retire old event types 11996410 11996411 12997610 12997611  
  
! 2019-03-04- Chen Chen (MR !243)  
   add a new decfile for B0->D_s1(2536)+ D-:  
   + 11198090 : Bd_Ds2536D,DKpi,Kpipi=DecProdCut.dec  
  
! 2019/02/08 - Mark Smith (MR !219)  
   Add three new decfiles for the pptaunu analysis background cocktails with the correct gen cuts.  
   + 11876005 : Bd_D0ppbarX,Xmunu=TightCut.dec  
   + 12775004 : Bu_D+ppbarX,Xmunu=TightCut.dec  
   + 12877401 : Bu_Lcpipipbarmunu,pX=TightCut2.dec  
  
! 2019-02-18 - Mikkel Bjoern (MR !229)  
   Added new dec files with correct decay descriptor of B decay in TightCut tool for GGSZ modes  
   + 12165149 : Bu_D0K,KSKK=TightCut,LooserCuts,PHSP,fixArrow.dec  
   + 12165147 : Bu_D0K,KSpipi=TightCut,LooserCuts,PHSP,fixArrow.dec  
   + 12165148 : Bu_D0pi,KSKK=TightCut,LooserCuts,PHSP,fixArrow.dec  
   + 12165146 : Bu_D0pi,KSpipi=TightCut,LooserCuts,PHSP,fixArrow.dec  
   + 12167193 : Bu_D0Kst+,KSpipi,KSpi=DecProdCut.dec  
   Added link to dec file overview to README.md  
  
! 2019-02-25 - Dana Bobulska (MR !233)  
   Added two new dec files related to doubly charmed baryon searches:  
     + 26165858 - Xicc_Xic0pi,pKKpi-res=GenXicc,DecProdCut,WithMinPTv1  
     + 26165859 - Omegacc_Omegac0pi,pKKpi=GenXicc,DecProdCut,WithMinPTv2  
  
! 2019-02-222- Chen Chen (MR !228)  
   add a new decfile for B02DDKpi with tight cut m(Kpi)<850 MeV  
   + 11198080 : Bd_DDKpi,Kpipi=TightCut,mKpiCut850MeV.dec  
  
! 2019-02-14 - Fabrice Desse (MR !223) 
   Added new decay file  
   + 11124411 : Bd_Ksteta,e+e-g=Dalitz,DecProdCut,TightCut60MeV.dec  
  
! 2019-02-17 - S. Blusk (MR !226)  
   Added 1 new decay file for Xib- SL decay with longer Xic0 lifetime  
   + 16875035 : Xib_Xic0munu,pKKpi=cocktail,tau=155fs.dec  
  
! 2019-02-18 - Qungnian Xu (MR !227)  
   Add 3 new decay files  
   + 28144005 : psi2S,Jpsipipi=DecProdCut.dec  
   + 28144004 : psi2S,Jpsipipi=VVpipi,DecProdCut.dec  
   + 28112001 : psi2S,mm=DecProdCut.dec  
  

