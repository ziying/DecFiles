# EventType: 11584023
#
# Descriptor: [B0 -> (D- -> K+ pi- e- anti-nu_e) e+ nu_e]cc
# NickName: Bd_Denu,Kpienu=VisibleInAcceptance,HighVisMass,EvtGenDecayWithCut,BESIIIModel
# Cuts: LoKi::GenCutTool/DecProdCut
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[(B0 => (D- => K+ pi- e- nu_e~) e+ nu_e)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[B0]cc' : "visMass"  }
# evtgendecay.HighVisMass.Preambulo += [
#     "visMass = ( ( GMASS ( 'e+' == GID , 'e-' == GID, 'K+' == GABSID, 'pi+' == GABSID ) ) > 4500 * MeV )",
# ]
#
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'DecProdCut')
# tightCut = gen.SignalRepeatedHadronization.DecProdCut
# tightCut.Decay   = '[(B0 => (D- => ^K+ ^pi- ^e- nu_e~) ^e+ nu_e)]CC'
# tightCut.Cuts    =    {
#     '[K+]cc'     : "inAcc",
#     '[pi-]cc'    : "inAcc",
#     '[e+]cc'     : "inAcc",
#     '[e-]cc'     : "inAcc" }
# tightCut.Preambulo += [
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " 
#     ]
# EndInsertPythonCode
#
# Documentation: D chain background for B0 -> Kpiee,
# selected to have a visible mass larger than 4.5 GeV
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Maxime Schubiger
# Email: maxime.schubiger@cern.ch
# Date: 20190822
# CPUTime: < 1 min 
#
Alias        MyD-              D-
Alias        MyD+              D+
ChargeConj   MyD-              MyD+
#
Decay B0sig
#HQET2 parameter as for B->Dlnu, taken from Spring 2019 HFLAV averages:
#https://hflav-eos.web.cern.ch/hflav-eos/semi/spring19/html/ExclusiveVcb/exclBtoD.html
1.000        MyD- e+ nu_e             PHOTOS HQET2 1.131 1.081; #rho^2 as of HFLAV 2019 Spring, v1 unchanged 
Enddecay
CDecay anti-B0sig
#
Decay MyD-
1.0  K+ pi- e- anti-nu_e  DToKpienu;
Enddecay
CDecay MyD+
#
End
#
