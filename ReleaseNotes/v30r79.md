DecFiles v30r79 2022-10-05 
==========================  
 
! 2022-10-05 - Michal Kreps (MR !1126)  
   Add 2 new decay files  
   + 15144002 : Lb_JpsipK,mm=phsp,LHCbAcceptance  
   + 15114013 : Lb_pKmumu=phsp,LHCbAcceptance  
  
! 2022-09-19 - Gabriel Matthew Nowak (MR !1124)  
   Add new decay file  
   + 41900012 : ttbar_bb,2l=1l15GeV  
  
! 2022-09-16 - Federica Borgato (MR !1123)  
   Add 8 new decay files  
   + 15498003 : Lb_Lc2593Ds,Lcpipi,ppiK,pipipi=DecProdCut  
   + 15498001 : Lb_Lc2593Ds,Lcpipi,ppiK=DecProdCut  
   + 15498203 : Lb_Lc2593Dsst,Lcpipi,ppiK,pipipi=DecProdCut  
   + 15498201 : Lb_Lc2593Dsst,Lcpipi,ppiK=DecProdCut  
   + 15498002 : Lb_Lc2625Ds,Lcpipi,pipipi=DecProdCut  
   + 15498000 : Lb_Lc2625Ds,Lcpipi=DecProdCut  
   + 15498202 : Lb_Lc2625Dsst,Lcpipi,pipipi=DecProdCut  
   + 15498200 : Lb_Lc2625Dsst,Lcpipi=DecProdCut  
  
! 2022-09-15 - Nico Kleijne (MR !1122)  
   Add new decay file  
   + 27165905 : Dst_D0pi,KSpipi=res,TightCut,LooserCuts  
  
! 2022-09-13 - Shiyang Li (MR !1121)  
   Add 2 new decay files  
   + 15164133 : Lb_LambdacK,LambdaK=DecProdCut  
   + 15164132 : Lb_Lambdacpi,LambdaK=DecProdCut  
  
! 2022-09-11 - Bo Fang (MR !1120)  
   Add 3 new decay files  
   + 12997626 : B+_DstXc,Xc2hhhNneutrals_cocktail_a1pi0,upto5prongs=TightCut  
   + 11896626 : Bd_DstXc,Xc2hhhNneutrals_cocktail_a1pi0,upto5prongs=TightCut  
   + 13996624 : Bs_DstXc,Xc2hhhNneutrals_cocktail_a1pi0,upto5prongs=TightCut  
  
! 2022-09-08 - Jessy Daniel (MR !1119)  
   Add 8 new decay files  
   + 12165537 : Bu_D0K,Kst0Rho0,Kspipipi0=TightCut,SVV,HELAMP001  
   + 12165538 : Bu_D0K,Kst0Rho0,Kspipipi0=TightCut,SVV,HELAMP010  
   + 12165539 : Bu_D0K,Kst0Rho0,Kspipipi0=TightCut,SVV,HELAMP100  
   + 12165534 : Bu_D0K,KstRho,Kspipipi0=TightCut,SVV,HELAMP001  
   + 12165535 : Bu_D0K,KstRho,Kspipipi0=TightCut,SVV,HELAMP010  
   + 12165536 : Bu_D0K,KstRho,Kspipipi0=TightCut,SVV,HELAMP100  
   + 12165533 : Bu_D0Pi,Kst0Rho0,Kspipipi0=TightCut,SVV,HELAMP001  
   + 12165532 : Bu_D0Pi,KstRho,Kspipipi0=TightCut,SVV,HELAMP001  
  
! 2022-05-23 - Louis Henry (MR !1084)  
   Add 2 new decay files  
   + 13144103 : Bs_JpsiKS,mm=TightCut,OnlyT  
   + 15144106 : Lb_JpsiLambda,mm=phsp,DecProdCut,TightCut,OnlyT  
  
