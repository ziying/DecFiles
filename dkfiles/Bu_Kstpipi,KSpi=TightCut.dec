# EventType: 12105182
#
# Descriptor: [B+ -> (K*(892)+ -> (KS0 -> pi+ pi-) pi+) pi+ pi-]cc
#
# NickName: Bu_Kstpipi,KSpi=TightCut
#
# Documentation: B+ to K*+ pi+ pi- with K*+ forced into KS0 pi+, and the KS0 into pi+ pi-
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: John Smeaton
# Email: john.smeaton@cern.ch
# Date: 20210312
# CPUTime: <1min
#
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[B+ ==> ^(K*(892)+ ==> ^KS0 ^pi+) ^pi+ ^pi- ]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import centimeter" ,
#     "inAcc = in_range ( 0.010, GTHETA, 0.400 ) & in_range   ( 1.8 , GETA , 5.0 )", 
#     "goodForRich = in_range ( 3.0 * GeV , GP , 150 * GeV )",
#     "ksInAcc = in_range ( 2 , GETA , 5 )",
#     "ksPion =  in_range ( 1.6 , GETA ,  5.2 ) & in_range ( 2 * GeV , GP ,  150 * GeV )",
#     "bothPI =  2 == GNINTREE (  ( 'pi+' == GABSID )   & ksPion )",
#     "ksTT   =  GVEV & ( GFAEVX ( GVZ , 1.e+10 ) < 240 * centimeter )",
#     "good_KS = ksInAcc & bothPI & ksTT",
#     "had_pt = GPT > 300 * MeV",
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'       : 'inAcc & goodForRich & had_pt',
#     '[KS0]cc'       : 'good_KS & had_pt',
#     '[K*(892)+]cc'  : 'had_pt',
# }
# EndInsertPythonCode
#
#
Alias      MyKst+    K*+
Alias      MyKst-    K*-
ChargeConj MyKst+    MyKst-
Alias      MyKs      K_S0
ChargeConj MyKs      MyKs
#
Decay B+sig
  1.000   MyKst+ pi+ pi-   PHSP;
Enddecay
CDecay B-sig
#
Decay MyKst+
  1.000   MyKs pi+         VSS;
Enddecay
CDecay MyKst-
#
Decay MyKs
  1.000   pi+ pi-          PHSP;
Enddecay
#
End
#
