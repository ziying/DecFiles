# EventType: 15564014
#
# Descriptor: [Lambda_b0 -> ( D- -> K+ pi- pi-) pi+  H_30 ]cc
#
# NickName: Lambda0_PsiDMDpi,D_pipiK=FullGenEvtCut,mPsiDM=3000MeV
#
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/LbtoDpiDM
#
# Documentation:
#    Decay a L0 to a pi D -> Kpipi and a redefined H_30 for our need, acting the latter as stable Dark Matter candidate of 3.00 GeV.
# EndDocumentation
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: 2 min
# Responsible: Saul Lopez
# Email: saul.lopez.solino@cern.ch
# Date: 20210927
#
#
# InsertPythonCode:
# from Configurables import LHCb__ParticlePropertySvc, LoKi__GenCutTool
# LHCb__ParticlePropertySvc().Particles = [
# "H_30     89       36      0.0     3.000000        1.000000e+16    A0      36      0.00"
# ]
# ## Generator level cuts:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "LbtoDpiDM" )
# tracksInAcc = Generation().LbtoDpiDM
# tracksInAcc.Code = " count ( isGoodLb ) > 0 "
# ### - HepMC::IteratorRange::descendants   4
# tracksInAcc.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodKaon = ( ( GPT > 0.15*GeV ) & inAcc & ( 'K+' == GABSID ) )"
#                          , "isGoodPi   = ( ( GPT > 0.15*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodD    = ( ( 'D+' == GABSID ) & ( GNINTREE(isGoodPi, 1) >1 ) & ( GNINTREE( isGoodKaon, 1 ) > 0 ) )"
#                          , "isGoodLb   = ( ( 'Lambda_b0' == GABSID ) & ( GNINTREE( isGoodD, 1 ) > 0 ) & ( GNINTREE( isGoodPi, 1 ) > 0 ) )" ]
# EndInsertPythonCode
#
Alias      MyD-      D-
Alias      MyD+      D+
ChargeConj MyD-    MyD+
#
Alias  MyH_30     A0
Alias  Myanti-H_30    A0
ChargeConj MyH_30   Myanti-H_30
#
Decay Lambda_b0sig
    1.000    MyD-     pi+   MyH_30    PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD-
    1.000    K+    pi-   pi-          D_DALITZ;
Enddecay
CDecay MyD+
#
End
