#!/bin/tcsh

set DIR = $PWD/dkfiles

set OUT = CheckGenCutTool.md

rm -rf $OUT

echo >> $OUT
echo "# Date = "`date` >> $OUT
echo >> $OUT

set TOT = 0
set COUNT = 0

foreach SAMPLE ( `ls $DIR | grep dec` )
    if ( `grep LoKi__GenCutTool $DIR/$SAMPLE | wc -l` != 0 ) then
        if ( `grep -i TightCut $DIR/$SAMPLE | grep -e "->" | wc -l` != 0 ) then
            echo "# $SAMPLE" >> $OUT
            echo `grep -i EventType $DIR/$SAMPLE | sed s:"#"::`"  " >> $OUT
            echo `grep -i email $DIR/$SAMPLE | sed s:"#"::`"  " >> $OUT
            echo `grep -i TightCut $DIR/$SAMPLE | grep -e "->" | grep -v Descriptor | grep -v Documentation | sed s:"#"::`"  " >> $OUT
            echo >> $OUT
            @ COUNT++
        endif
    endif
    @ TOT++
end

echo >> $OUT
echo "# COUNT / TOT = $COUNT / $TOT" >> $OUT
echo >> $OUT

cat $OUT
