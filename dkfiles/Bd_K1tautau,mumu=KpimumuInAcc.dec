# EventType: 11614450
#
# NickName: Bd_K1tautau,mumu=KpimumuInAcc
# Descriptor: {[[B0]nos -> (K_10 -> K+ pi- pi0) (tau+ -> mu+ nu_mu anti-nu_tau) (tau- -> mu- anti-nu_mu nu_tau)]cc, [[B0]os -> (K_1~0 -> K- pi+ pi0) (tau+ -> mu+ nu_mu anti-nu_tau) (tau- -> mu- anti-nu_mu nu_tau)]cc}
# 
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# kpimumuInAcc = Generation().SignalRepeatedHadronization.TightCut
# kpimumuInAcc.Decay = '[B0 ==> ^mu+ ^mu- nu_mu nu_mu~ K+ pi- pi0 nu_tau nu_tau~]CC'
# kpimumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.010, GTHETA, 0.400))',
#     'onePionInAcc = (GNINTREE( ("pi-"==GABSID) & inAcc) >= 1)',
#     'oneKaonInAcc = (GNINTREE( ("K-"==GABSID) & inAcc) >= 1)'
#     ]
# kpimumuInAcc.Cuts = {
#     '[B0]cc'    : 'onePionInAcc & oneKaonInAcc',
#     '[mu+]cc'   : 'inAcc'
#     }
#
# EndInsertPythonCode
#
# Documentation: B0 -> K1 pi tau tau decays with K1 -> K+ pi- pi0 decays.
# EndDocumentation
#
# CPUTime: < 1min
#
# PhysicsWG: RD 
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20221026
#
Alias      MyK1_1270        K_10 
Alias      Myanti-K1_1270   anti-K_10
ChargeConj MyK1_1270        Myanti-K1_1270
#
Alias      MyK1_1400        K'_10
Alias      Myanti-K1_1400   anti-K'_10
ChargeConj MyK1_1400        Myanti-K1_1400
#
Alias      MyK*1430_0       K_0*0
Alias      Myanti-K*1430_0  anti-K_0*0
ChargeConj Myanti-K*1430_0  MyK*1430_0
#
Alias      MyK*1430_+       K_0*+
Alias      MyK*1430_-       K_0*-
ChargeConj MyK*1430_-       MyK*1430_+
#
Alias      MyK*0            K*0
Alias      Myanti-K*0       anti-K*0
ChargeConj Myanti-K*0       MyK*0
#
Alias      Myrho-           rho-
Alias      Myrho+           rho+
ChargeConj Myrho+           Myrho-
#
Alias      MyK*+            K*+
Alias      MyK*-            K*-
ChargeConj MyK*-            MyK*+
# 
Alias      Mytau+           tau+
Alias      Mytau-           tau-
ChargeConj Mytau+           Mytau-
#
Decay B0sig
  0.500    MyK1_1270        Mytau+     Mytau-          BTOSLLBALL;
  0.500    MyK1_1400        Mytau+     Mytau-          BTOSLLBALL;
Enddecay
CDecay anti-B0sig
#
Decay MyK1_1270
  0.07     MyK*0            pi0                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.14     MyK*+            pi-                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.25     Myrho-           K+                         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.19     MyK*1430_+       pi-                        VSS;
  0.09     MyK*1430_0       pi0                        VSS;
  0.12     K+          pi-  pi0                        PHSP;
Enddecay
CDecay Myanti-K1_1270
#
Decay MyK1_1400
  0.31     MyK*0            pi0                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.63     MyK*+            pi-                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.02     Myrho-           K+                         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Myanti-K1_1400
#
Decay MyK*0
  1.000    K+               pi-                        VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyK*1430_0
  1.000    K+               pi-                        PHSP;
Enddecay
CDecay Myanti-K*1430_0
#
Decay MyK*+
  1.000    K+               pi0                        VSS;
Enddecay
CDecay MyK*-
#
Decay MyK*1430_+
  1.000    K+               pi0                        PHSP;
Enddecay
CDecay MyK*1430_-
#
Decay Myrho-
  1.000    pi-              pi0                        VSS;
Enddecay
CDecay Myrho+
#
Decay Mytau+
  1.000     mu+             nu_mu      anti-nu_tau     TAULNUNU;
Enddecay
CDecay Mytau-
#
End
#
