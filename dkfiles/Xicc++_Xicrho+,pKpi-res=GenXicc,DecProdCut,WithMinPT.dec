# EventType:  26164460
#
# Descriptor: [Xi_cc++ -> (Xi_c+ -> p K- pi+) rho+ ]cc
#
# NickName: Xicc++_Xicrho+,pKpi-res=GenXicc,DecProdCut,WithMinPT
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 2000*MeV
#
# CPUTime: < 1 min
#
# Documentation: Xicc++ decay to Xi_c+ rho+ by phase space model, Xi_c resonances included.
# all daughters of Xicc are required to be in the acceptance of LHCb 
# and the Xicc PT is required to be larger than 2000 MeV. 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Wenqian Huang
# Email: wenqian.huang@cern.ch
# Date: 20200101
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
Alias MyXi_c+ Xi_c+
Alias Myanti-Xi_c- anti-Xi_c-
ChargeConj MyXi_c+ Myanti-Xi_c-
#
Alias      Myrho+     rho+
Alias      Myrho-     rho-
ChargeConj Myrho+     Myrho-
#
Decay Xi_cc++sig
  1.000    MyXi_c+   Myrho+           PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
Decay MyXi_c+
  0.116000000 p+      Myanti-K*0                              PHSP;
  0.094000000 p+      K-      pi+                             PHSP;
Enddecay
CDecay Myanti-Xi_c-
#
Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Myrho-
  1.000        pi-        pi0            VSS;
Enddecay
CDecay Myrho+
#
End
#
