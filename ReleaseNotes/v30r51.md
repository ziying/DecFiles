!========================= 2020-09-25 DecFiles v30r51 =======================  
 
! 2020-09-25 - Michal Kreps (MR !587)  
   Treat also decay file removal in script for release notes  
  
! 2020-09-24 - William Barter (MR !586)  
   Modify 10 decay files  
   + 49000065 : dijet=b,m110GeV,mu  
   + 49000070 : dijet=c,m70GeV  
   + 49000072 : dijet=c,m110GeV  
   + 49000073 : dijet=c,m70GeV,mu  
   + 49000075 : dijet=c,m110GeV,mu  
   + 49000080 : dijet=q,m70GeV  
   + 49000082 : dijet=q,m110GeV  
   + 49000060 : dijet=b,m70GeV  
   + 49000062 : dijet=b,m110GeV  
   + 49000063 : dijet=b,m70GeV,mu  
   Remove 2 decay files  
   + 49000083 : dijet=q,m80GeV,mu  
   + 49000085 : dijet=q,m100GeV,mu  
  
! 2020-09-22 - Jeremy Peter Dalseno (MR !583)  
   Add 6 new decay files  
   + 12103406 : Bu_rho0rho+,pi+pi-pi+pi0=DecProdCut,PHSP,Charmless  
   + 12103443 : Bu_phirho+,K+K-pi+pi0=DecProdCut,PHSP,Charmless  
   + 12103444 : Bu_Kst0Kst+,K+K-pi+pi0=DecProdCut,PHSP,Charmless  
   + 12103445 : Bu_phiKst+,K+K-K+pi0=DecProdCut,PHSP,Charmless  
   + 12103422 : Bu_Kst0rho+,K+pi-pi+pi0=DecProdCut,PHSP,Charmless  
   + 12103423 : Bu_rho0Kst+,K+pi-pi+pi0=DecProdCut,PHSP,Charmless  
  
! 2020-09-18 - Michal Kreps (MR !582)  
   Improve script for preparing release notes

! 2020-09-18 - Svende Annelies Braun (MR !580)  
   Add 8 new decay files  
   + 12893600 : Bu_D0DX,muX=cocktail,RDstar,TightCut  
   + 11894210 : Bd_Dst+DsX,taunu=cocktail,RDstar,TightCut  
   + 11894600 : Bd_D0DX,muX=cocktail,RDstar,TightCut  
   + 12893610 : Bu_D0DsX,taunu=cocktail,RDstar,TightCut  
   + 12895000 : Bu_Dst+DsX,taunu=cocktail,RDstar,TightCut  
   + 12895400 : Bu_Dst+DX,muX=cocktail,RDstar,TightCut  
   + 11894610 : Bd_Dst+DX,muX=cocktail,RDstar,TightCut  
   + 11894200 : Bd_D0DsX,taunu=cocktail,RDstar,TightCut  
  
! 2020-09-17 - Federico Betti (MR !578)  
   Add 10 new decay files  
   + 13774210 : Bs_DsX,KKpi=cocktail,TightCut,ACPKKCuts  
   + 11493220 : Bd_DsX,KKpi=cocktail,TightCut,ACPKKCuts  
   + 12865420 : Bu_D+X,Kpipi=cocktail,TightCut,ACPKKCuts  
   + 12893200 : Bu_DsX,KKpi=cocktail,TightCut,ACPKKCuts  
   + 11493300 : Bd_DsX,KSK=cocktail,TightCut,ACPKKCuts  
   + 12893300 : Bu_DsX,KSK=cocktail,TightCut,ACPKKCuts  
   + 11774010 : Bd_D+X,Kpipi=cocktail,TightCut,ACPKKCuts  
   + 13774300 : Bs_DsX,KSK=cocktail,TightCut,ACPKKCuts  
   + 12865530 : Bu_D+X,KSpi=cocktail,TightCut,ACPKKCuts  
   + 11774110 : Bd_D+X,KSpi=cocktail,TightCut,ACPKKCuts  
  
! 2020-09-11 - Xabier Cid Vidal (MR !574)  
   Add 3 new decay files  
   + 21113041 : D+_pi+etap,mumu=DecProdCut  
   + 23115004 : Ds_pi+etap,pipimumu,etapModel=DecProdCut  
   + 21113421 : D+_pi+eta,mumu=DecProdCut  
  
! 2020-09-04 - Benedetto Gianluca Siddi (MR !573)  
   Modify 2 decay files  
   + 23903000 : incl_b=Ds,KKpi,3pi=DDALITZ,DecProdCut,ExtraParticles  
   + 21263005 : incl_b=D+,Kpipi,3pi=DDALITZ,DecProdCut,ExtraParticles  
  
