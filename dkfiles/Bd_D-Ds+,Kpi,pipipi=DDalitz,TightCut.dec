# EventType: 11196018
#
# Descriptor: [B0 -> (D_s+ -> pi+ pi- pi+) (D- -> K+ pi- pi-)]cc
#
# NickName: Bd_D-Ds+,Kpi,pipipi=DDalitz,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[( [B0]cc => ^(D+ => K- pi+ pi+) ^(D_s- => pi- pi+ pi-) ) ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#     ,"CS  = LoKi.GenChild.Selector"
#     ,'goodDp = ( (GP>12000*MeV) & (GPT>1500*MeV) & ( (GCHILD(GPT,("K+" == GABSID )) > 1400*MeV) & (GCHILD(GP,("K+" == GABSID )) > 5000*MeV) & in_range ( 0.010 , GCHILD(GTHETA,("K+" == GABSID )) , 0.400 ) ) & ( (GCHILD(GPT,CS("[D+ => K- ^pi+ pi+ ]CC")) > 200*MeV) & (GCHILD(GP,CS("[D+ => K- ^pi+ pi+ ]CC")) > 1600*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[D+ => K- ^pi+ pi+ ]CC")) , 0.400 ) ) & ( (GCHILD(GPT,CS("[D+ => K- pi+ ^pi+ ]CC")) > 200*MeV) & (GCHILD(GP,CS("[D+ => K- pi+ ^pi+ ]CC")) > 1600*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[D+ => K- pi+ ^pi+ ]CC")) , 0.400 ) )  )'
#     ,"goodDs = ( ( (GCHILD(GPT,1) > 200*MeV) & (GCHILD(GP,1) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,1) , 0.400 ) ) & ( (GCHILD(GPT,2) > 200*MeV) & (GCHILD(GP,2) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,2) , 0.400 ) ) & ( (GCHILD(GPT,3) > 200*MeV) & (GCHILD(GP,3) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,3) , 0.400 ) ) )"
# ]
# tightCut.Cuts = {
#      '[D+]cc': 'goodDp'
#     ,'[D_s-]cc': 'goodDs'
#     }
# EndInsertPythonCode
#
# Documentation: B0 -> (D+ -> Kpipi) (Ds -> 3pi). Daughters in LHCb Acceptance and passing StrippingB0d2DTauNuForB2XTauNuAllLines cuts. Ds->3pi with D_DALITZ.
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: < 1 min
# Responsible: A. Romero Vidal
# Email: antonio.romero@usc.es
# Date:   20200629
#
Alias      MyD_s+  D_s+
Alias      MyD_s-  D_s-
ChargeConj MyD_s+  MyD_s-

Alias      MyD-        D-
Alias      MyD+        D+
ChargeConj MyD-        MyD+

Decay B0sig
  1.000     MyD-       MyD_s+               PHSP;
Enddecay
CDecay anti-B0sig

Decay MyD_s+
  1.000     pi-          pi+        pi+     D_DALITZ;
Enddecay
CDecay MyD_s-

Decay MyD-
  1.000     pi-          K+         pi-     D_DALITZ;
Enddecay
CDecay MyD+
#
End

