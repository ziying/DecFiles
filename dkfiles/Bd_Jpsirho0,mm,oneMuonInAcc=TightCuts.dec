# EventType: 11144009
#
# Descriptor: {[B0 -> (J/psi(1S) -> mu+ mu-) (rho0 -> pi+ pi-)]cc}
#
# NickName: Bd_Jpsirho0,mm,oneMuonInAcc=TightCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
#  
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay      = ' Beauty  => ( J/psi(1S) => ^mu+ ^mu-)  ( rho(770)0 => ^pi- ^pi+ )'
# tightCut.Preambulo += [
#     "inAcc          = in_range ( 0.005 , GTHETA , 0.400 ) " , 
#     "goodMuplus    = GINTREE( ('mu+' == GID) & inAcc )",
#     "goodMuminus   = GINTREE( ('mu-' == GID) & inAcc )", 
#     "goodPiplus    = GINTREE( ('pi+' == GID) & inAcc )", 
#     "goodPiminus   = GINTREE( ('pi-' == GID) & inAcc )"
#     	]
# tightCut.Cuts       =    {
#     "Beauty": " ((goodMuplus) | (goodMuminus)) & (goodPiplus) & (goodPiminus) " }
# EndInsertPythonCode
#
# Documentation: DecFile for B0 -> J/psi rho0 decays, with both pions in the acceptance and at least one muon.
# EndDocumentation
#
# CPUTime: <1 min
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Michel De Cian
# Email: michel.de.cian@cern.ch
# Date: 20190521
#
Define Hp 0.159
Define Hz 0.775
Define Hm 0.612
Define pHp 1.563
Define pHz 0.0
Define pHm 2.712
#
Alias      MyJ/psi    J/psi
Alias      Myrho0      rho0
ChargeConj Myrho0      Myrho0
ChargeConj MyJ/psi    MyJ/psi
#
Decay B0sig
  1.000         MyJ/psi   Myrho0          SVV_HELAMP Hp pHp Hz pHz Hm pHm;
Enddecay
Decay anti-B0sig
  1.000         MyJ/psi   Myrho0     SVV_HELAMP Hm pHm Hz pHz Hp pHp;
Enddecay
#
Decay MyJ/psi
  1.000         mu+       mu-            PHOTOS VLL;
Enddecay
#
Decay Myrho0
  1.000         pi+        pi-            VSS;
Enddecay
#
End	

