# EventType: 26197974
#
# Descriptor: [Sigma_c*+ ->  (Lambda_c+ -> p+ K- pi+) (D+ -> K- pi+ pi+) pi-]cc
#
# NickName: Pc4550,LcpiD+,pkpi=TightCut,InAcc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Sample: SignalRepeatedHadronization
#
# ParticleValue: "Sigma_c*+ 486 4214 1.0 4.550 6.591074e-23 Sigma_c*+ 4214 0.00" , "Sigma_c*~- 487 -4214  -1.0  4.550  6.591074e-23 anti-Sigma_c*-       -4214  0.00"
#
# Documentation: Pc decay to Lambda_c+ D+ pi- in PHSP model with daughters in LHCb Acceptance
# Sigma_c*+ used for the generation. Neutrals flag is set to 9 to allow numbering.
#
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal     = generation.SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[Sigma_c*+ => ^(Lambda_c+ ==> ^p+ ^K- ^pi+) ^(D+ ==> ^K- ^pi+ ^pi+) ^pi-]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'GY           =  LoKi.GenParticles.Rapidity () ## to be sure ' ,
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'fastTrack    =  ( GPT > 220 * MeV ) & ( GP  > 3.0 * GeV )   ' ,
#     'goodTrack    =  inAcc & inEta                               ' ,
#     'goodLc       =  ( GPT > 0.9 * GeV )   ' ,
#     'goodD        =  ( GPT > 0.9 * GeV )   ' ,
# ]
# tightCut.Cuts     =    {
#     '[Lambda_c+]cc'  : 'goodLc   ' ,
#     '[K-]cc'         : 'goodTrack & fastTrack' ,
#     '[pi+]cc'        : 'goodTrack & fastTrack' ,
#     '[p+]cc'         : 'goodTrack & fastTrack & ( GP > 9 * GeV ) ',
#     '[D+]cc'         : 'goodD' ,
#     }
# EndInsertPythonCode
#
# PhysicsWG:   Onia
# Tested:      Yes
# Responsible: Gary Robertson
# Email:       gary.robertson@ed.ac.uk
# Date:        20210721
# CPUTime:     <1min
#
#
Alias      MyD+       D+
Alias      MyD-       D-
ChargeConj MyD+    MyD-
#
Alias            MyLambda_c+        Lambda_c+
Alias       anti-MyLambda_c-   anti-Lambda_c-
ChargeConj       MyLambda_c+ anti-MyLambda_c-
#
Decay Sigma_c*+sig
  1.000          MyLambda_c+     MyD+    pi-      PHSP;
Enddecay
CDecay anti-Sigma_c*-sig
#
Decay MyLambda_c+
  1.000          p+      K-      pi+    PHSP;
Enddecay
CDecay anti-MyLambda_c-
#
Decay MyD+
  1.000          K-      pi+     pi+    PHSP;
Enddecay
CDecay MyD-
#
End
#
