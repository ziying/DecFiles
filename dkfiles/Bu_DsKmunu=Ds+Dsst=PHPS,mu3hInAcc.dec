# EventType: 12575200
# Descriptor: {[[B+] => (D_s- => K+ K- pi-) K+ nu_mu mu+]cc}
# NickName: Bu_DsKmunu=Ds+Dsst=PHPS,mu3hInAcc
# Cuts: BeautyTomuCharmTo3h
# CutsOptions: MuonPMin 2.5*GeV HadronPtMin 0.25*GeV HadronPMin 4.5*GeV
# FullEventCuts: LoKi::FullGenEventCut/TightCuts
#
# InsertPythonCode:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "TightCuts" )
# tightCuts = Generation().TightCuts
# tightCuts.Code = "( count ( DsDaughPt ) > 0 )"
# tightCuts.Preambulo += [
# "from GaudiKernel.SystemOfUnits import GeV",
# "DsDaughPt     = (('D_s-' == GABSID) & (GCHILD(GPT,1)+GCHILD(GPT,2)+GCHILD(GPT,3) > 2.0*GeV))"
#  ]
# EndInsertPythonCode
#
# CPUTime: <1 min
# Documentation: Sum of B+ -> DsKmunu and B+->Ds*Kmunu for background study on R(Ds(*)). 
#                Requires that the mu from beauty and the 3h from charm are in acc. 
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Stefano Cali
# Email: stefano.cali@cern.ch
# Date: 20190703
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      MyD_s*+    D_s*+
Alias      MyD_s*-    D_s*-
ChargeConj MyD_s*+    MyD_s*-
#
Decay B+sig  
  0.5   MyD_s-   K+   mu+    nu_mu       PHOTOS PHSP;
  0.5   MyD_s*-  K+   mu+    nu_mu       PHOTOS PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s+
  0.0545     K+    K-     pi+          PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
  0.935   MyD_s+  gamma               PHOTOS VSP_PWAVE;
  0.058   MyD_s+  pi0                 PHOTOS VSS;
Enddecay
CDecay MyD_s*-
#
End
