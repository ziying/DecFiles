# EventType: 12112003
#
# Descriptor: [B+ -> K+ (tau- -> TAUOLA) mu+]cc
# 
#
# NickName: Bu_Ktaumu,3pipi0=DecProdCut,TightCutFixed,tauola8,phsp
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[ B+ ==> ^([mu+]CC) ([tau- ==> ^pi+ ^pi- ^pi- pi0 nu_tau]CC) ^K+ ]CC'
# tightCut.Cuts      =    {
#     '[pi-]cc'   : ' goodPion  ' ,
#     '[K+]cc'    : ' goodKaon  ' ,
#     '[mu+]cc'   : ' goodMuon  ' ,
#     '[B+]cc'    : ' goodB  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV',
#     'inAcc = in_range( 0.005, GTHETA, 0.400)',
#     'goodMuon  = ( GP  > 2500  * MeV ) & inAcc' ,
#     'goodKaon  = ( GPT > 650  * MeV ) & inAcc' ,
#     'goodPion  = ( GPT > 220  * MeV ) & inAcc' ,
#     'goodB     = ( GPT > 2500  * MeV ) ' ]
#
# EndInsertPythonCode
#
#
# Documentation:  Bu decay to K tau mu
# Tau lepton decay in the 3-prong charged pion plus a pi0, using Tauola 8.
# Phase-space decay for B
# All final-state products but the pi0 in the acceptance.
# Tight generator level cuts applied for the B and all final state particles but the pi0 
# EndDocumentation
#
# PhysicsWG: RD
#
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Alison Tully
# Email: alison.tully@cern.ch
# Date: 20190719
#

# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias         Mytau+     tau+
Alias         Mytau-     tau-
ChargeConj    Mytau+     Mytau-
#
Decay B+sig
  0.500       K+      Mytau+     mu-        PHSP;
  0.500       K+      mu+        Mytau-     PHSP;
Enddecay
CDecay B-sig
#
Decay Mytau-
  1.00        TAUOLA 8;
Enddecay
CDecay Mytau+
#
End
