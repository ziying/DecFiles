from Configurables import Generation
from Gaudi.Configuration import *

Generation().PileUpTool = "FixedLuminosityForRareProcess"

importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )

from Configurables import Special, PythiaProduction, Pythia8Production

Generation().addTool( Special )
Generation().Special.addTool( PythiaProduction  )
Generation().Special.addTool( Pythia8Production )

Generation().Special.Pythia8Production.Commands += [
  "WeakSingleBoson:ffbar2gmZ = on", # Z0/gamma* production
  "23:mMin = 20.",                   # min mass of Z0 in GeV
  "23:mMax = 40.",                   # min mass of Z0 in GeV
  "TimeShower:mMaxGamma = 20.",      # max inv mass in photon conversion
  "PhaseSpace:mHatMin = 20.",        # constrain inv mass
  "23:onMode = off",                # turn it off
  "23:onIfMatch = 13 -13",          # turn it on for the decay to muon final state only
]
