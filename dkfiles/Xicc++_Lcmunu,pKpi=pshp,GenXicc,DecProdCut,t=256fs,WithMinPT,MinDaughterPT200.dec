# EventType:  26674061
#
# Descriptor: [Xi_cc++ -> (Lambda_c+ -> p+ K- pi+) mu+ nu_mu]cc
#
# NickName: Xicc++_Lcmunu,pKpi=pshp,GenXicc,DecProdCut,t=256fs,WithMinPT,MinDaughterPT200
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 2000*MeV MinDaughterPT 200*MeV
#
# CPUTime: < 1 min
#
# ParticleValue: "Xi_cc++                506        4422   2.0      3.62140000      2.560000e-13                    Xi_cc++        4422      0.00000000", "Xi_cc~--               507       -4422  -2.0      3.62140000      2.560000e-13               anti-Xi_cc--       -4422      0.00000000"
#
# Documentation: Xicc++ decay to Lambda_c+ mu+ nu by phase space model, Lambda_c resonances included,
# the mass of Xi_cc++ is set to 3.6214 GeV, the lifetime is set to 256 fs.
# All daughters of Xicc are required to be in the acceptance of LHCb and with PT>200 MeV 
# and the Xicc PT is required to be larger than 2000 MeV. 
# EndDocumentation

#
# PhysicsWG: Charm 
# Tested: Yes
# Responsible: Miroslav Saur
# Email: miroslav.saur@cern.ch
# Date: 20210114
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
Alias      MyDelta++  Delta++
Alias      Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++  Myanti-Delta--
#
Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-
#
Decay Xi_cc++sig
  1.000   MyLambda_c+   mu+   nu_mu           PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
#
Decay MyLambda_c+
  0.008600000 MyDelta++ K-                                    PHSP;
  0.010700000 p+      Myanti-K*0                              PHSP;
  0.025400000 p+      K-      pi+                             PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyDelta++
1.0000    p+  pi+                     PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
End
#
