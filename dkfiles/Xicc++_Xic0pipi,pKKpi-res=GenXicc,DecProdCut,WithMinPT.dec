# EventType: 26166063
#
# Descriptor: [Xi_cc++ -> (Xi_c0 -> p+ K- K- pi+) pi+ pi+]cc
#
# NickName: Xicc++_Xic0pipi,pKKpi-res=GenXicc,DecProdCut,WithMinPT
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 3000*MeV MinDaughterPT 200*MeV
#
# CPUTime: < 1 min
#
# ParticleValue: "Xi_cc++  506  4422  2.0  3.62155  0.256e-12  Xi_cc++ 4422 0.0", "Xi_cc~--  507  -4422  -2.0  3.62155  0.256e-12  anti-Xi_cc--  -4422  0.0", "Xi_c0 106 4132 0.0 2.47088 1.55e-13 Xi_c0 4132 0.0", "Xi_c~0 107 -4132 0.0 2.47088 1.55e-13 anti-Xi_c0 -4132 0.0"
#
# Documentation: Xicc++ decay to Xic0 pi+ pi+ by phase space model,
# Xic0 decay to p+ K- K- pi+ (p+ K- K*(892)0 resonance included),
# all daughters of Xicc++ are required to be in the acceptance of LHCb and with minimum PT 200 MeV,
# and the Xicc++ PT is required to be larger than 3000 MeV,
# Xic0 is required to be generated with the lifetime of 155fs,
# Xicc++ is required to be generated with the lifetime of 256fs.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Ao Xu
# Email: ao.xu@cern.ch
# Date: 20200221
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
Alias      MyXic0              Xi_c0
Alias      MyantiXic0          anti-Xi_c0
ChargeConj MyXic0              MyantiXic0
#
Decay Xi_cc++sig
  1.000    MyXic0   pi+   pi+                         PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
Decay MyXic0
  0.50     p+       K-    Myanti-K*0                  PHSP;
  0.50     p+       K-    K-          pi+             PHSP;
Enddecay
CDecay MyantiXic0
#
Decay MyK*0
  1.000    K+       pi-                               VSS;
Enddecay
CDecay Myanti-K*0
#
End
#

