# EventType: 27263478
# 
# Descriptor: { [D*+ -> (D0 -> (rho0 -> pi- pi+) (eta -> gamma gamma) ) pi+]cc, [D*+ -> (D0-> pi- (a_0+ -> pi+ (eta -> gamma gamma)) ) pi+]cc, [D*+ -> (D0 -> pi- pi+ (eta -> gamma gamma) ) pi+]cc }
#
# NickName: Dst_D0pi,pipieta=TightCut,tighter,Coctail
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#   Decay file for D* -> D0 pi+
#   where D0 decays to mode (pi- pi+ eta)
#   with incoherent resonance Coctail based on Dalitz from Belle arXiv:2106.04286
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Jolanta Brodzicka
# Email: Jolanta.Brodzicka@cern.ch
# Date: 20220110
# CPUTime: <1min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ D*(2010)+ => ^( D0 ==> pi- pi+ ( eta -> gamma gamma ) ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV     ',
#     'inAcc       = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) )',
#     'goodD0Eta   = ( GINTREE( ("gamma"==GABSID) & (GPT > 1200 * MeV) & inAcc & inCaloAcc ) )',
#     'goodD0Pim   = ( ("pi-"==GABSID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0Pip   = ( ("pi+"==GABSID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0      = ( (GPT > 1600 * MeV) & GINTREE(goodD0Pim) & GINTREE(goodD0Pip) & GINTREE(goodD0Eta) )'
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'  : 'inAcc ',
#     '[D0]cc'   : 'goodD0 '
#     }
# EndInsertPythonCode

Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
#
Alias      Myrho0 rho0
ChargeConj Myrho0 Myrho0
#
#
Alias      Mya0+ a_0+
Alias      Mya0- a_0-
ChargeConj Mya0+ Mya0-
#
Alias      Myeta  eta
ChargeConj Myeta Myeta
#
Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig
#
Decay MyD0
0.33   Myrho0 Myeta  SVS;
0.33   pi- Mya0+  PHSP;
0.33   pi- pi+ Myeta PHSP;
Enddecay
CDecay MyantiD0
#
Decay Myrho0
1.0   pi- pi+      VSS;
Enddecay
#
Decay Mya0+
1.0   pi+ Myeta      PHSP;
Enddecay
CDecay Mya0-
#
Decay Myeta
1.0     gamma gamma      PHSP;
Enddecay
#
End
 
