# EventType: 11102453
#
# Descriptor: [Beauty -> (K*(892)0 -> K+ pi-) (pi0 -> gamma gamma)]cc
#
# NickName: Bd_Kstpi0=TightCut,Kst982width100,HighPtPi0 
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[B0 => (K*(892)0 => ^K+ ^pi-) ^pi0]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'        : 'inAcc' , 
#     '[pi-]cc'       : 'inAcc' , 
#     'pi0'           : 'goodPi0'
#     }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) " , 
#     "goodPi0       = ('pi0' == GABSID) & ( GPT > 3.0 * GeV ) & InEcal",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) ",
#     ]  
#
# EndInsertPythonCode
#
# Documentation: Bkgd for B->K*G, all in PHSP, Kpi in acceptance, high PT pi0
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20210927
#
#
Alias      Mypi0        pi0
ChargeConj Mypi0        Mypi0
#
Alias       MyK*0      K*0
Alias       Myanti-K*0   anti-K*0
ChargeConj  MyK*0        Myanti-K*0
#

LSNONRELBW MyK*0
BlattWeisskopf MyK*0 0.0
Particle MyK*0 0.895 0.06
ChangeMassMin MyK*0 0.795
ChangeMassMax MyK*0 0.995

LSNONRELBW Myanti-K*0
BlattWeisskopf Myanti-K*0 0.0
Particle Myanti-K*0 0.895 0.06
ChangeMassMin Myanti-K*0 0.795
ChangeMassMax Myanti-K*0 0.995
#
Decay B0sig
  1.000   MyK*0      Mypi0         PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyK*0
  1.000   K+         pi-       PHSP;
Enddecay
CDecay Myanti-K*0 
#
Decay Mypi0
  1.000        gamma      gamma           PHSP;
Enddecay
#
End
