# EventType: 11166144
#
# Descriptor: [B0 -> (D*(2010)- -> (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) pi-) pi+]cc
#
# NickName: Bd_Dst-pi,KSpipi=PartRecCut
#
# Cuts: 'LoKi::GenCutTool/TightCut'
#
# Documentation:
# B_0 decay D*( D0(K0s pi- pi+) pi-) pi+ . Tight cuts adapted for partially reconstructed decays in B2Kspipipi AmAn
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# Responsible: Pablo Baladron Rodriguez
# Email: pablo.baladron.rodriguez@cern.ch
# Date: 20211016
# CPUTime: 2 min
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '^[B0 => (D*(2010)- => (D~0 => (KS0 => pi+ pi-) pi+ pi-) pi-) pi+]CC'
# tightCut.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodPi      = ( ( GPT > 0.5*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodPiKs      = ( ( GP > 2.*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodKs      = ( ( 'KS0' == GABSID ) & (GNINTREE( isGoodPiKs, 1 ) > 1 ))"
#                          , "isGoodD0        = ( ( 'D0' == GABSID ) & ( GNINTREE( isGoodPi, 1 ) > 0 ) & ( GNINTREE( isGoodKs, 1 ) > 0 ))"
#                          , "isGoodDstr        = ( ( 'D*(2010)+' == GABSID ) & ( GNINTREE( isGoodPi, 1 ) > 0 ) & ( GNINTREE( isGoodD0, 1 ) > 0 ))"
#                          , "isGoodB        = ( ( 'B0' == GABSID ) & ( GNINTREE( isGoodPi, 1 ) > 0 ) & ( GNINTREE( isGoodDstr, 1 ) > 0 ))" ]
# tightCut.Cuts	= {
#	'[B0]cc' : 'isGoodB'}

# EndInsertPythonCode
#
Alias MyD*+       D*+
Alias MyD*-       D*-
ChargeConj MyD*+  MyD*-
Alias MyD0        D0
Alias Myanti-D0   anti-D0
ChargeConj MyD0   Myanti-D0
Alias myK_S0      K_S0
ChargeConj myK_S0 myK_S0
##
Decay B0sig
  1.000    MyD*-  pi+    SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyD*+
  1.000    MyD0       pi+          VSS;
Enddecay
Decay MyD*-
  1.000    Myanti-D0  pi-          VSS;
Enddecay
#
Decay MyD0
  1.000     myK_S0 pi+  pi-        PHSP;
Enddecay
CDecay Myanti-D0
#
Decay myK_S0
  1.000     pi+  pi-               PHSP;
Enddecay
#
End


