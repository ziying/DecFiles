# Powheg options for ggF->H->ZZ production
from Configurables import Generation
from Gaudi.Configuration import *

Generation().PileUpTool = "FixedLuminosityForRareProcess"

importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )

from Configurables import Special, PowhegProduction

Generation().addTool( Special )
Generation().Special.addTool( PowhegProduction )

# Powheg options.
Generation().Special.addTool(PowhegProduction)
Generation().Special.PowhegProduction.Process = "gg_H_quark-mass-effects"
Generation().Special.PowhegProduction.Commands += [
    "lhans1 10770", # Change the first proton PDF.
    "lhans2 10770", # Change the second proton PDF.
    "hfact    104.16d0",
    "runningscale 0",
    "massren 0",
    "zerowidth 1",
    "ew 1",
    "model 0",
    "gfermi 0.116637D-04",
    "hdecaymode -1",
    "masswindow 10d0",
    "hmass 125",
    "hwidth 3.605D-03",
    "topmass 172.5",
    "bottommass 4.75d0",
    "hdecaywidth 0"
]
