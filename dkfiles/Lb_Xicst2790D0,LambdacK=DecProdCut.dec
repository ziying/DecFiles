# EventType: 15196020
# NickName: Lb_Xicst2790D0,LambdacK=DecProdCut
# Descriptor: [Lambda_b0 -> (Xi_c*0 -> (Lambda_c+ -> p+ K- pi+) K- ) (anti-D0 -> K+ pi-) ]cc
#
# Documentation:
#   Decay Lambda_b0 -> Xi_c*0(2790) D~0 with Xi_c*0 --> Lambda_c+ K-, Lambda_c+ --> p K- pi+, D~0 --> K+ pi-
#   Daughters in LHCb Acceptance
# EndDocumentation
#
# Cuts: DaughtersInLHCb
# CPUTime: < 1 min
#
# ###             NAME       GEANTID PDGID CHARGE MASS(GeV) TLIFE(s)   EVTGENNAME  PYTHIAID  MAXWIDTH(GeV)
# ParticleValue: "Xi_c*0      494     4314   0.0    2.793    5.0e-023   Xi_c*0      4314      0.010",       "Xi_c*~0  107  -4314  0.0 2.793  5.0e-023  anti-Xi_c*0  -4314  0.010"
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Alessio Piucci
# Email: alessio.piucci@cern.ch
# Date: 20180815
#

Alias MyXi_c0 Xi_c*0
Alias Myanti-Xi_c0 anti-Xi_c*0
ChargeConj MyXi_c0 Myanti-Xi_c0

Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-

Alias      MyD0          D0
Alias      Myanti-D0     anti-D0
ChargeConj MyD0          Myanti-D0

# force the Lb to decay to Xi_c*0 and D0
Decay Lambda_b0sig
  1.000    MyXi_c0 Myanti-D0         PHSP;
Enddecay
CDecay anti-Lambda_b0sig

# force the Xi_c*0 to decay to Lc and K-
Decay MyXi_c0
  1.000   MyLambda_c+ K-             PHSP;
Enddecay
CDecay Myanti-Xi_c0

Decay MyLambda_c+
  1.000   p+ K- pi+                  PHSP;
Enddecay
CDecay Myanti-Lambda_c-

Decay Myanti-D0
  1.000   K+ pi-                     PHSP;
Enddecay
CDecay MyD0

#
End
#
