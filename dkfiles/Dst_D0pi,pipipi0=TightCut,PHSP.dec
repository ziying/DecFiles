# EventType: 27163406
#
# Descriptor: [D*(2010)+ -> (D0 -> pi- pi+ pi0) pi+]cc
#
# NickName: Dst_D0pi,pipipi0=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[D*(2010)+ -> ^(D0 -> ^pi- ^pi+ pi0) pi+]CC'
# tightCut.Cuts      =    {
#     '[D0]cc'    : ' goodD & goodKaon & goodMuon    ' ,
#     '[D*(2010)+]cc'   : ' goodDst & goodSlowPion  ', }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) & in_range( 1.9 , GETA , 5.0 )' ,
#     'goodMuon  = GCHILDCUT(( GPT > 500  * MeV ) & ( GP > 6 * GeV ) & inAcc, 2)   ' ,
#     'goodKaon  = GCHILDCUT(( GPT > 300  * MeV ) & ( GP > 2 * GeV ) & inAcc, 1)   ' ,
#     'goodPion  = ( GPT > 150  * MeV )                        & inAcc   ' ,
#     'goodSlowPion  = GCHILDCUT (  ( GPT > 300  * MeV ) & ( GP > 1000  * MeV ) & inAcc , "Charm =>  Charm ^(pi+|pi-)"  )',
#     'goodD     = ( GPT > 250  * MeV ) & ( GP > 3 * GeV )', 
#     'goodDst   = ( GPT > 500  * MeV ) & ( GP > 1 * GeV )', ]
#
# EndInsertPythonCode
#
# Documentation:
#   This is the decay file for the decay D* -> D0 pi+
#   D0 decays to pi- pi+ pi0 with a D* tag. Tight cuts.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Lorenzo Capriotti
# Email: lorenzo.capriotti@cern.ch
# Date: 20180926
# CPUTime: < 1 min
#

Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0

Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig

Decay MyD0
  1.000   pi- pi+ pi0  PHSP;
Enddecay
CDecay MyantiD0
#
End
 
