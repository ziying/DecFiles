# EventType: 15574036
#
# Descriptor: [Lambda_b0 -> (D0 -> pi- mu+ nu_mu) p+ K-]cc
#
# NickName: Lb_D0pK,pimunu=DecProdCut,HighVisMass,EvtGenCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(Lambda_b0 => (D0 => pi- mu+ nu_mu) p+ K-)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[Lambda_b0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += ["visMass  = ( ( GMASS ( 'pi-' == GABSID , 'mu+' == GABSID, 'p+' == GABSID, 'K-' == GABSID) ) > 4500 * MeV ) " ]
#
# EndInsertPythonCode
#
# Documentation:  Decfile created as bg for Lb2Lambda1520mue analysis.
# D0 forced into pi- munu, phase space decay for Lambda_b0.
# Generator level cut applied to have a visible mass larger than 4.5 GeV.
# EndDocumentation
#
# CPUTime: 2 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Dan Thompson   
# Email: dan.thompson@cern.ch
# Date: 20211125
#
Alias      MyD0          D0
Alias      Myanti-D0     anti-D0
ChargeConj MyD0          Myanti-D0
#
Decay Lambda_b0sig
  1.000    MyD0  p+  K-     PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD0
  1.000    pi-  mu+  nu_mu                 ISGW2;
Enddecay
CDecay Myanti-D0
#
End
#
